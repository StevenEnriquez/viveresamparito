package viveresamparito.formadepago.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.auditoria.managers.ManagerAuditoria;
import viveresamparito.model.formadepago.manager.ManagerFormadepago;
import minimarketdemo.model.core.entities.FormaDePago;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanFormadepago implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerFormadepago managerTipoDocumento;
	private List<FormaDePago> listaformadepago;
	private FormaDePago formadepago;
	public ManagerFormadepago getManagerTipoDocumento() {
		return managerTipoDocumento;
	}

	public void setManagerTipoDocumento(ManagerFormadepago managerTipoDocumento) {
		this.managerTipoDocumento = managerTipoDocumento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private boolean panelColapsado;
	private FormaDePago formadepagoseleccionado;

	@PostConstruct
	public void inicializar() {
		listaformadepago = managerTipoDocumento.findAllTipoDeDocumentos();
		formadepago= new FormaDePago();
		panelColapsado = true;
	}

	public void actionListenerColapsarPanel() {

		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarTipoDocumento() {
		try {
			managerTipoDocumento.insertarTipoDocumento(formadepago); 
			listaformadepago = managerTipoDocumento.findAllTipoDeDocumentos();
			formadepago = new FormaDePago();
			JSFUtil.crearMensajeINFO("Datos de estudiante insertados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	

	public void actionListenerEliminarTipoDocumento(int documento) {
		managerTipoDocumento.eliminarTipoDocumento(documento);
		listaformadepago = managerTipoDocumento.findAllTipoDeDocumentos();
		JSFUtil.crearMensajeINFO("Estudiante eliminado");

	}

	public void actionListenerSeleccionarTipoDocumento(FormaDePago tipodocumento) {
		formadepagoseleccionado= tipodocumento;

	}
	public void actionListenerActualizarTipoDocumento() {
		try {
			managerTipoDocumento.actualizarTipoDocumento(formadepagoseleccionado);
			listaformadepago=managerTipoDocumento.findAllTipoDeDocumentos();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}

	public List<FormaDePago> getListaformadepago() {
		return listaformadepago;
	}

	public void setListaformadepago(List<FormaDePago> listaformadepago) {
		this.listaformadepago = listaformadepago;
	}

	public FormaDePago getFormadepagoseleccionado() {
		return formadepagoseleccionado;
	}

	public void setFormadepagoseleccionado(FormaDePago formadepagoseleccionado) {
		this.formadepagoseleccionado = formadepagoseleccionado;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public FormaDePago getFormadepago() {
		return formadepago;
	}
 
	public void setFormadepago(FormaDePago formadepago) {
		this.formadepago = formadepago;
	}

}
