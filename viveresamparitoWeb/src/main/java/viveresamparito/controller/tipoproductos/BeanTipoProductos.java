package viveresamparito.controller.tipoproductos;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import minimarketdemo.controller.JSFUtil;
import viveresamparito.model.core.managers.ManagerTipoProductos;
import minimarketdemo.model.core.entities.TipoProducto;

@Named
@SessionScoped
public class BeanTipoProductos implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerTipoProductos managerViveres;
    private List<TipoProducto> listaViveres;
    private TipoProducto tipoproducto;
    private boolean panelColapsado;
    private TipoProducto tipoproductoseleccionado;
    @PostConstruct
    public void inicializar() {
    	listaViveres=managerViveres.findAllTipoProductos();
        tipoproducto=new TipoProducto();
    	panelColapsado=true;
    }
public void actionListenerColapsarPanel() {
    	
    	panelColapsado=!panelColapsado;
    }
public void actionListenerInsertarProducto() {
	try {
		managerViveres.insertarProducto(tipoproducto);
		listaViveres=managerViveres.findAllTipoProductos();
		tipoproducto=new TipoProducto();
		JSFUtil.crearMensajeINFO("Datos del Producto insertados");
	} catch (Exception e) {
		JSFUtil.crearMensajeERROR(e.getMessage());
		e.printStackTrace();
	}
}
public void actionListenerSeleccionarTipoProducto(TipoProducto tipoproducto) {
    tipoproductoseleccionado = tipoproducto;

}
public void actionListenerActualizarTipoProducto() {
    try {
        managerViveres.actualizarTipoProducto(tipoproductoseleccionado);
        listaViveres=managerViveres.findAllTipoProductos();
        JSFUtil.crearMensajeINFO("Datos actualizados");
    } catch (Exception e) {
    	JSFUtil.crearMensajeERROR(e.getMessage());
        e.printStackTrace();
    }

}

	public BeanTipoProductos() {
		//TODO Auto-generated constructor stub
	}
	public List<TipoProducto> getLiTipoProductos() {
		return listaViveres;
	}
	public ManagerTipoProductos getManagerTipoProductos() {
		return managerViveres;
	}
	public void setManagerTipoProductos(ManagerTipoProductos managerTipoProductos) {
		this.managerViveres = managerTipoProductos;
	}
	public List<TipoProducto> getListatipoproductos() {
		return listaViveres;
	}
	public void setListatipoproductos(List<TipoProducto> listatipoproductos) {
		this.listaViveres = listatipoproductos;
	}

	public TipoProducto getTipoproducto() {
		return tipoproducto;
	}
	public void setTipoproducto(TipoProducto tipoproducto) {
		this.tipoproducto = tipoproducto;
	}
	
	public TipoProducto getTipoproductoseleccionado() {
		return tipoproductoseleccionado;
	}
	public void setTipoproductoseleccionado(TipoProducto tipoproductoseleccionado) {
		this.tipoproductoseleccionado = tipoproductoseleccionado;
	}
	
	public ManagerTipoProductos getManagerViveres() {
		return managerViveres;
	}
	public void setManagerViveres(ManagerTipoProductos managerViveres) {
		this.managerViveres = managerViveres;
	}
	public List<TipoProducto> getListaViveres() {
		return listaViveres;
	}
	public void setListaViveres(List<TipoProducto> listaViveres) {
		this.listaViveres = listaViveres;
	}
	public boolean isPanelColapsado() {
		return panelColapsado;
	}
	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	
	public void actionListenerEliminarProducto(int idTipoProducto) {
		managerViveres.eliminarProducto(idTipoProducto);
		listaViveres = managerViveres.findAllTipoProductos();
		JSFUtil.crearMensajeINFO("Producto eliminado");

	}
	
}
