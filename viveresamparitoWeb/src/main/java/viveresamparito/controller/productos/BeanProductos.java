package viveresamparito.controller.productos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import minimarketdemo.controller.JSFUtil;
import viveresamparito.model.producto.managers.*;
import minimarketdemo.model.core.entities.ProProducto;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoProducto;

@Named
@SessionScoped
public class BeanProductos implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerProductos managerViveres;
	
	
	private int idproductoseleccionado;
	private Date fechaseleccionada;
	private int idtipoproductoseleccionado;
	private String idproveedorseleccionado;
	
	
	private List<ProProducto> listaViveres;
	private List<ProProveedor> listaProveedores;
	private List<TipoProducto> listaTipoProducto;
 	private ProProducto producto;
	private boolean panelColapsado;
	private ProProducto productoseleccionado;

	@PostConstruct
	public void inicializar() {
		listaViveres = managerViveres.findAllProProductos();
		listaTipoProducto=managerViveres.findAllTipoProductos();
		listaProveedores=managerViveres.findAllProProveedores();
		producto = new ProProducto();
		panelColapsado = true;
	}
	public void actionListenerColapsarPanel() {

		panelColapsado = !panelColapsado;
	}
	
	public List<String> completeTextProducto(String query) {
		String queryLowerCase = query.toLowerCase();
		List<String> prodList = new ArrayList<>();
		for (TipoProducto prod : listaTipoProducto) {
			prodList.add(prod.getDescripcion());
		}
		return prodList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}

	
	public List<String> completeTextProducto2(String query) {
		String queryLowerCase = query.toLowerCase();
		List<String> prodList = new ArrayList<>();
		for (ProProveedor prod : listaProveedores) {
			prodList.add(prod.getNombreComercial());
		}
		return prodList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}
	
	
	public void actionListenerInsertarProductos() {
			managerViveres.insertarProductos(producto, idtipoproductoseleccionado, idproveedorseleccionado);
			listaViveres=managerViveres.findAllProProductos();
			producto=new ProProducto();
			JSFUtil.crearMensajeINFO("Datos del Producto insertados");
		
	}
	public void actionListenerSeleccionarProductos(ProProducto producto) {
		productoseleccionado = producto;

	}

	public void actionListenerActualizarProducto() {
		try {
			managerViveres.actualizarProductos(productoseleccionado);
			listaViveres = managerViveres.findAllProProductos();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}
	public void actionListenerEliminarProducto(int descripcion) {
		managerViveres.eliminarProducto(descripcion);
		listaViveres = managerViveres.findAllProProductos();
		JSFUtil.crearMensajeINFO("Producto eliminado");

	}
	

	public BeanProductos() {
		// TODO Auto-generated constructor stub
	}

	public List<ProProducto> getListaViveres() {
		return listaViveres;
	}

	public ProProducto getProducto() {
		return producto;
	}

	public void setProducto(ProProducto producto) {
		this.producto = producto;
	}

	public void setListaViveres(List<ProProducto> listaViveres) {
		this.listaViveres = listaViveres;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	public ManagerProductos getManagerViveres() {
		return managerViveres;
	}

	public void setManagerViveres(ManagerProductos managerViveres) {
		this.managerViveres = managerViveres;
	}

	
	public ProProducto getProductoseleccionado() {
		return productoseleccionado;
	}

	public void setProductoseleccionado(ProProducto productoseleccionado) {
		this.productoseleccionado = productoseleccionado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getIdproductoseleccionado() {
		return idproductoseleccionado;
	}

	public void setIdproductoseleccionado(int idproductoseleccionado) {
		this.idproductoseleccionado = idproductoseleccionado;
	}

	public Date getFechaseleccionada() {
		return fechaseleccionada;
	}

	public void setFechaseleccionada(Date fechaseleccionada) {
		this.fechaseleccionada = fechaseleccionada;
	}

	public List<ProProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<ProProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public List<TipoProducto> getListaTipoProducto() {
		return listaTipoProducto;
	}

	public void setListaTipoProducto(List<TipoProducto> listaTipoProducto) {
		this.listaTipoProducto = listaTipoProducto;
	}

	public int getIdtipoproductoseleccionado() {
		return idtipoproductoseleccionado;
	}

	public void setIdtipoproductoseleccionado(int idtipoproductoseleccionado) {
		this.idtipoproductoseleccionado = idtipoproductoseleccionado;
	}

	public String getIdproveedorseleccionado() {
		return idproveedorseleccionado;
	}

	public void setIdproveedorseleccionado(String idproveedorseleccionado) {
		this.idproveedorseleccionado = idproveedorseleccionado;
	}

	
	
}
