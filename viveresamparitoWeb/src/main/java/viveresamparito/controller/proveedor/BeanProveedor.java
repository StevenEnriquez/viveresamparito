package viveresamparito.controller.proveedor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.CiuCiudad;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoDeDocumento;
import viveresamparito.model.proveedor.managers.ManagerProveedor;

@Named
@SessionScoped
public class BeanProveedor implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerProveedor managerProveedores;
	private int idproveedorseleccionado;
	private Date fechaseleccionada;
	private int idtipodocumentoseleccionado;
	private int idciudadseleccionado;

	private List<ProProveedor> listaproveedores;
	private List<TipoDeDocumento> listaTipoDeDocumentos;
	private List<CiuCiudad> listaCiudades;
	private ProProveedor proveedor;
	private boolean panelColapsado;
	private ProProveedor provedorseleccionado;

	@PostConstruct

	public void inicializar() {
		listaproveedores = managerProveedores.findAllProProveedors();
		listaTipoDeDocumentos = managerProveedores.findAllTipoDeDocumentos();
		listaCiudades = managerProveedores.findAllCiuCiudads();
		proveedor = new ProProveedor();
		panelColapsado = true;
	}
	public void actionListenerColapsarPanel() {

		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarProductos() {
		managerProveedores.insertarProductos(proveedor, idtipodocumentoseleccionado, idciudadseleccionado);
		listaproveedores = managerProveedores.findAllProProveedors();
		proveedor = new ProProveedor();
		JSFUtil.crearMensajeINFO("Datos del Proveedor insertados");

	}

	public void actionListenerEliminarProeedor(String documento) {
		managerProveedores.eliminarProducto(documento);
		listaproveedores = managerProveedores.findAllProProveedors();
		JSFUtil.crearMensajeINFO("Proveedor eliminado");

	}

	public void actionListenerSeleccionarProveedor(ProProveedor proveedors) {
		provedorseleccionado = proveedors;

	}

	public void actionListenerActualizarProveedor() {
		JSFUtil.crearMensajeINFO("Datos actualizados");
		try {
			managerProveedores.actualizarProveedor(provedorseleccionado);
			listaproveedores = managerProveedores.findAllProProveedors();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		JSFUtil.crearMensajeINFO("Datos actualizados");
	}
	public BeanProveedor() {
		//TODO Auto-generated constructor stub
	}
	public ManagerProveedor getManagerProveedores() {
		return managerProveedores;
	}
	public void setManagerProveedores(ManagerProveedor managerProveedores) {
		this.managerProveedores = managerProveedores;
	}
	public int getIdproveedorseleccionado() {
		return idproveedorseleccionado;
	}
	public void setIdproveedorseleccionado(int idproveedorseleccionado) {
		this.idproveedorseleccionado = idproveedorseleccionado;
	}
	public Date getFechaseleccionada() {
		return fechaseleccionada;
	}
	public void setFechaseleccionada(Date fechaseleccionada) {
		this.fechaseleccionada = fechaseleccionada;
	}
	public int getIdtipodocumentoseleccionado() {
		return idtipodocumentoseleccionado;
	}
	public void setIdtipodocumentoseleccionado(int idtipodocumentoseleccionado) {
		this.idtipodocumentoseleccionado = idtipodocumentoseleccionado;
	}
	public int getIdciudadseleccionado() {
		return idciudadseleccionado;
	}
	public void setIdciudadseleccionado(int idciudadseleccionado) {
		this.idciudadseleccionado = idciudadseleccionado;
	}
	public List<ProProveedor> getListaproveedores() {
		return listaproveedores;
	}
	public void setListaproveedores(List<ProProveedor> listaproveedores) {
		this.listaproveedores = listaproveedores;
	}
	public List<TipoDeDocumento> getListaTipoDeDocumentos() {
		return listaTipoDeDocumentos;
	}
	public void setListaTipoDeDocumentos(List<TipoDeDocumento> listaTipoDeDocumentos) {
		this.listaTipoDeDocumentos = listaTipoDeDocumentos;
	}
	public List<CiuCiudad> getListaCiudades() {
		return listaCiudades;
	}
	public void setListaCiudades(List<CiuCiudad> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}
	public ProProveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(ProProveedor proveedor) {
		this.proveedor = proveedor;
	}
	public boolean isPanelColapsado() {
		return panelColapsado;
	}
	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	public ProProveedor getProvedorseleccionado() {
		return provedorseleccionado;
	}
	public void setProvedorseleccionado(ProProveedor provedorseleccionado) {
		this.provedorseleccionado = provedorseleccionado;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
