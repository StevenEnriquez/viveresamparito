package viveresamparito.controller.ciudad;


import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.CiuCiudad;

import viveresamparito.model.ciudad.managers.ManagerCiudad;

@Named
@SessionScoped
public class BeanCiudad implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCiudad managerCiudad;
	private List<CiuCiudad> listaCiudad;
	private CiuCiudad ciudad;
	private boolean panelColapsado;
	private CiuCiudad ciudadseleccionada;

	@PostConstruct
	public void inicializar() {
		listaCiudad = managerCiudad.findAllCiudad();
		ciudad = new CiuCiudad();
		panelColapsado=true;

	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado=!panelColapsado;
	}

	public void actionListenerInsertarCiudad() {
		try {
			managerCiudad.insertarCiudad(ciudad);
			listaCiudad = managerCiudad.findAllCiudad();
			ciudad = new CiuCiudad();
			JSFUtil.crearMensajeINFO("Datos insertados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
	}
	
	public void actionListenerEliminarCiudad(int codigoCiudad) {
		managerCiudad.eliminarCiudad(codigoCiudad);
		listaCiudad=managerCiudad.findAllCiudad();
		JSFUtil.crearMensajeINFO("Ciudad eliminada");
	}
	
	public void actionListenerSeleccionarCiudad(CiuCiudad ciudad) {
	    ciudadseleccionada = ciudad;

	}
	
	public void actionListenerActualizarCiudad() {
	    try {
	        managerCiudad.actualizarCiudad(ciudadseleccionada);
	        listaCiudad=managerCiudad.findAllCiudad();
	        JSFUtil.crearMensajeINFO("Datos actualizados");
	    } catch (Exception e) {
	    	JSFUtil.crearMensajeERROR(e.getMessage());
	        e.printStackTrace();
	    }
	}
	public ManagerCiudad getManagerCiudad() {
		return managerCiudad;
	}

	public void setManagerCiudad(ManagerCiudad managerCiudad) {
		this.managerCiudad = managerCiudad;
	}

	public List<CiuCiudad> getListaCiudad() {
		return listaCiudad;
	}

	public void setListaCiudad(List<CiuCiudad> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public CiuCiudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiuCiudad ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public CiuCiudad getCiudadseleccionada() {
		return ciudadseleccionada;
	}

	public void setCiudadseleccionada(CiuCiudad ciudadseleccionada) {
		this.ciudadseleccionada = ciudadseleccionada;
	}
	
	
	
	
	
	

	
	
	
	
	

}
