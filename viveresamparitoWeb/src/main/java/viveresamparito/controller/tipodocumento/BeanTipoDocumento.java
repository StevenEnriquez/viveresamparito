package viveresamparito.controller.tipodocumento;

import java.io.Serializable;


import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.TipoDeDocumento;
import viveresamparito.model.tipodocumento.manager.ManagerTipoDocumento;

@Named
@SessionScoped
public class BeanTipoDocumento implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerTipoDocumento managerTipoDocumento;
	private List<TipoDeDocumento> listaTipoDocumentos;
	private TipoDeDocumento tipodocumento;
	private boolean panelColapsado;
	private TipoDeDocumento tipodocumentoseleccionado;

	@PostConstruct
	public void inicializar() { 
		listaTipoDocumentos = managerTipoDocumento.findAllTipoDeDocumentos();
		tipodocumento = new TipoDeDocumento();
		panelColapsado = true;
	}

	public void actionListenerColapsarPanel() {

		panelColapsado = !panelColapsado;
	}

	public void actionListenerInsertarTipoDocumento() {
		try {
			managerTipoDocumento.insertarTipoDocumento(tipodocumento);
			listaTipoDocumentos = managerTipoDocumento.findAllTipoDeDocumentos();
			tipodocumento = new TipoDeDocumento();
			JSFUtil.crearMensajeINFO("Datos de estudiante insertados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public BeanTipoDocumento() {
		// TODO Auto-generated constructor stub
	}

	public void actionListenerEliminarTipoDocumento(int documento) {
		managerTipoDocumento.eliminarTipoDocumento(documento);
		listaTipoDocumentos = managerTipoDocumento.findAllTipoDeDocumentos();
		JSFUtil.crearMensajeINFO("Estudiante eliminado");

	}

	public void actionListenerSeleccionarTipoDocumento(TipoDeDocumento tipodocumento) {
		tipodocumentoseleccionado = tipodocumento;

	}
	public void actionListenerActualizarTipoDocumento() {
		try {
			managerTipoDocumento.actualizarTipoDocumento(tipodocumentoseleccionado);
			listaTipoDocumentos=managerTipoDocumento.findAllTipoDeDocumentos();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}

	public List<TipoDeDocumento> getListaTipoDocumentos() {
		return listaTipoDocumentos;
	}

	public void setListaTipoDocumentos(List<TipoDeDocumento> listaTipoDocumentos) {
		this.listaTipoDocumentos = listaTipoDocumentos;
	}

	public TipoDeDocumento getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(TipoDeDocumento tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public TipoDeDocumento getTipodocumentoseleccionado() {
		return tipodocumentoseleccionado;
	}

	public void setTipodocumentoseleccionado(TipoDeDocumento tipodocumentoseleccionado) {
		this.tipodocumentoseleccionado = tipodocumentoseleccionado;
	}

}
