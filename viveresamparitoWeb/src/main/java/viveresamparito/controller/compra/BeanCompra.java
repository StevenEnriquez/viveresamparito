package viveresamparito.controller.compra;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.ComCompra;
import minimarketdemo.model.core.entities.ProProducto;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoProducto;
import minimarketdemo.model.core.utils.ModelUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import viveresamparito.model.comcompra.manager.ManagerCompra;
import viviresamparito.model.comcompra.dtos.DetalleCompraDTO;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@SessionScoped
public class BeanCompra implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCompra managerCompra;
	private List<ComCompra> listaCompras;
	private List<ProProveedor> listaProveedores;
	private List<DetalleCompraDTO> listaDetalleCompra;
	private List<TipoProducto> listaCategorias;
	private List<ProProducto> listaProductos;
	private String cedulaProveedorSeleccionado;
	private int idCatSeleccionada;
	private int idProSeleccionado;
	private int cantidad;
	private String mensaje;
	private ComCompra compra;
	private String nombreP;
	// Variables para pagina facturas
	private Date fechaInicio;
	private Date fechaFin;
	private DetalleCompraDTO detalleCompraDTO;

	public BeanCompra() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {

	}

	public void actionListenerAgregarDetalleCompra() {
		DetalleCompraDTO detalleCompra = new DetalleCompraDTO();
		detalleCompra.setIdPro(idProSeleccionado);
		ProProducto pro = new ProProducto();
		try {
			pro = managerCompra.findProductoById(idProSeleccionado);
			int id = 1;
			if (listaDetalleCompra.size() != 0) {
				id = listaDetalleCompra.get(listaDetalleCompra.size() - 1).getIdDetCom() + 1;
			}
			detalleCompra.setIdDetCom(id);
			detalleCompra.setNombrePro(pro.getNombre());
			detalleCompra.setCantidad(cantidad);
			listaDetalleCompra.add(detalleCompra);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}

	public String actionListenerFinalizarCompra() {
		Date fecha = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
		String fechaActual = format.format(fecha);
		if (listaDetalleCompra.size() > 0) {
			try {
				Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(fechaActual);
				compra.setFecha(date1);
				compra.setNumeroCompra(0);
				ProProveedor proveedor = managerCompra.findProveedorByCedula(cedulaProveedorSeleccionado);
				compra.setProProveedor(proveedor);
				managerCompra.registrarCompra(compra, listaDetalleCompra);
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
			}
		} else {
			JSFUtil.crearMensajeERROR("Lista de Compra Vacia.");
			return "";
		}
		return "generar_factura";
	}



	public void buscarPorCategoria() {
		listaProductos = managerCompra.findProductosByCatId(idCatSeleccionada, nombreP);
		if (listaProductos.size() < 1) {
			JSFUtil.crearMensajeERROR("0 Productos Econtrados");
		} else {
			JSFUtil.crearMensajeINFO(listaProductos.size() + " Productos Econtrados");
		}
	}

	public String actionListenerVerDetalles(int idCom) {
		try {
			ComCompra com = managerCompra.findCompraById(idCom);
			compra = new ComCompra();
			compra.setIdCompra(com.getIdCompra());
			compra.setFecha(com.getFecha());
			compra.setNumeroCompra(com.getNumeroCompra());
			listaDetalleCompra = new ArrayList<DetalleCompraDTO>();
			listaDetalleCompra = managerCompra.findDetalleComprasByComId(compra.getIdCompra());
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
		return "generar_factura";
	}

	public void actionListenerEliminarProductoDetalleCompra(int idDetCom) {
		listaDetalleCompra= managerCompra.eliminarProductoDetalleCompra(listaDetalleCompra, idDetCom);
	}
	public void actionEditarDetalleCompra(int id) {
		detalleCompraDTO= new DetalleCompraDTO();
		for(DetalleCompraDTO detalle:listaDetalleCompra) {
			if(id==detalle.getIdDetCom()) {
				detalleCompraDTO=detalle;
				break;			
			}
		}
	}
	
	public void actionActualizarCantidad() {
		int cont=0;
		for(DetalleCompraDTO detalle:listaDetalleCompra) {
			if(detalleCompraDTO.getIdDetCom()==detalle.getIdDetCom()) {
				detalle.setCantidad(detalleCompraDTO.getCantidad());
				listaDetalleCompra.set(cont, detalle);
				break;			
			}
			cont++;
		}
		JSFUtil.crearMensajeINFO("Cantidad Actualizada");
	}

	public String actionCargarMenuComprar() {
//		// obtener la fecha de ayer:
//		fechaInicio = ModelUtil.addDays(new Date(), -1);
//		// obtener la fecha de hoy:
//		fechaFin = new Date();
//		listaBitacora = managerAuditoria.findBit
//		return proveedores;acoraByFecha(fechaInicio, fechaFin);
//		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaBitacora.size());
		listaCategorias = managerCompra.findAllCategorias();
		listaProveedores = managerCompra.findAllProveedores();
		detalleCompraDTO= new DetalleCompraDTO();
		compra = new ComCompra();
		listaProductos = new ArrayList<ProProducto>();
		listaDetalleCompra= new ArrayList<DetalleCompraDTO>();
		nombreP = "";
		mensaje = "";
		return "comprar";
	}


	public String actionCargarMenuCompra() {
		// obtener la fecha de ayer:
		fechaInicio = ModelUtil.addDays(new Date(), -1);
		// obtener la fecha de hoy:
		fechaFin = new Date();
		listaCompras = managerCompra.findComprasByFecha(fechaInicio, fechaFin);
		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaCompras.size());
		return "compras";
	}

	public void actionListenerEliminarCompra(int idCom) {
		try {
			managerCompra.eliminarCompra(idCom);
			JSFUtil.crearMensajeINFO("Compra Eliminada.");
			listaCompras = managerCompra.findComprasByFecha(fechaInicio, fechaFin);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JSFUtil.crearMensajeERROR(e.getMessage());
		}
	}
	public void actionListenerBuscarCompras() {
		listaCompras = managerCompra.findComprasByFecha(fechaInicio, fechaFin);
	}
	public String actionReporte() {
	Map<String, Object> parametros = new HashMap<String, Object>();
	parametros.put("idCom", compra.getIdCompra());
	FacesContext context = FacesContext.getCurrentInstance();
	ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
	String ruta = servletContext.getRealPath("compra/compras/Reporte.jasper");
	System.out.println(ruta);
	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
	response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
	response.setContentType("application/pdf");
	try {
		Class.forName("org.postgresql.Driver");
		Connection connection = null;
		connection = DriverManager.getConnection("jdbc:postgresql://10.24.8.85:5432/epherrerag",
				"mipymes", "prueba21.");
		JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
		JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		context.getApplication().getStateManager().saveView(context);
		System.out.println("reporte generado.");
		context.responseComplete();
	} catch (Exception e) {
		JSFUtil.crearMensajeERROR(e.getMessage());
		e.printStackTrace();
	}
	return "";
}


	public DetalleCompraDTO getDetalleCompraDTO() {
		return detalleCompraDTO;
	}

	public void setDetalleCompraDTO(DetalleCompraDTO detalleCompraDTO) {
		this.detalleCompraDTO = detalleCompraDTO;
	}

	public String getCedulaProveedorSeleccionado() {
		return cedulaProveedorSeleccionado;
	}

	public void setCedulaProveedorSeleccionado(String cedulaProveedorSeleccionado) {
		this.cedulaProveedorSeleccionado = cedulaProveedorSeleccionado;
	}

	public List<ProProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<ProProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public List<ComCompra> getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(List<ComCompra> listaCompras) {
		this.listaCompras = listaCompras;
	}

	public List<DetalleCompraDTO> getListaDetalleCompra() {
		return listaDetalleCompra;
	}

	public void setListaDetalleCompra(List<DetalleCompraDTO> listaDetalleCompra) {
		this.listaDetalleCompra = listaDetalleCompra;
	}

	public List<TipoProducto> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<TipoProducto> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public List<ProProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public int getIdCatSeleccionada() {
		return idCatSeleccionada;
	}

	public void setIdCatSeleccionada(int idCatSeleccionada) {
		this.idCatSeleccionada = idCatSeleccionada;
	}

	public int getIdProSeleccionado() {
		return idProSeleccionado;
	}

	public void setIdProSeleccionado(int idProSeleccionado) {
		this.idProSeleccionado = idProSeleccionado;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ComCompra getCompra() {
		return compra;
	}

	public void setCompra(ComCompra compra) {
		this.compra = compra;
	}

	public String getNombreP() {
		return nombreP;
	}

	public void setNombreP(String nombreP) {
		this.nombreP = nombreP;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}	
}
	

