package viveresamparito.controller.ventas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import viveresamparito.model.comcompra.manager.ManagerCompra;
import viveresamparito.model.venventas.managers.ClienteDTO;
import viveresamparito.model.venventas.managers.DetalleVentaDTO;
import viveresamparito.model.venventas.managers.ManagerVentas;

import viviresamparito.model.comcompra.dtos.DetalleCompraDTO;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.CliCliente;
import minimarketdemo.model.core.entities.ComCompra;
import minimarketdemo.model.core.entities.FacFactura;
import minimarketdemo.model.core.entities.FormaDePago;
import minimarketdemo.model.core.entities.ProProducto;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoProducto;
import minimarketdemo.model.core.utils.ModelUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanVentas implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerVentas mc;
	private List<FacFactura> listaVentas;
	private List<DetalleVentaDTO> listaDetVentas;
	private List<CliCliente> listaClientes;
	private List<TipoProducto> listaCategorias;
	private List<FormaDePago> listaFormaDepago;
	private List<ProProducto> listaProductos;
	private String cedulaProveedorSeleccionado;
	private int idCatSeleccionada;
	private int idPro;
	private int idformadepago;
	private int cantidad;
	private double total;
	private String cedulacli;
	private String nombreempleado;
	private FacFactura compra;
	private String nombreP;
	private String NumeroFactura;
	private int numfactura;
	// Variables para pagina facturas
	private Date fechaInicio;
	private Date fechaFin;

	public BeanVentas() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct

	public void inicializar() {
//		Random r=new Random();
//		numfactura=r.nextInt(1,100)+1;
//		NumeroFactura="00"+numfactura;
		
		listaClientes = mc.findAllCliClientes();
		listaProductos = mc.findAllVtnProducto();
//		listaCompras = mc.findAllPrvCompraCabeceras();
		listaCategorias=mc.findAllTipoProductos();
		listaDetVentas = new ArrayList<DetalleVentaDTO>();
		listaFormaDepago=mc.findAllFormaDePagos();

	}

	public void actionListenerAgregarDetalleVenta() {
		listaDetVentas.add(mc.crearDetalleVenta(idPro, cantidad));
		JSFUtil.crearMensajeINFO("Producto agregado");
		total = mc.totalDetalleCompra(listaDetVentas);
		System.out.println("Guardado detalle");
	}

	public void actionListenerGuardarListadeCompras() {
		mc.productosMultiples(listaDetVentas, cedulacli, nombreempleado, idformadepago);
		JSFUtil.crearMensajeINFO("Compra Registrada");
		System.out.println("Compras guardada");
		listaDetVentas = new ArrayList<DetalleVentaDTO>();
		total = 0;
		inicializar();
	}

//	public void actionListenerAgregarDetalleCompra() {
//		DetalleVentaDTO detalleCompra = new DetalleVentaDTO();
//		detalleCompra.setIdProducto(idProSeleccionado);
//		ProProducto pro = new ProProducto();
//		try {
//			pro = managerCompra.findProductoById(idProSeleccionado);
//			int id = 1;
//			if (listaDetalleCompra.size() != 0) {
//				id = listaDetalleCompra.get(listaDetalleCompra.size() - 1).getIdProducto()+ 1;
//			}
////			detalleCompra.setIdDetCom(id);
//			detalleCompra.setIdProducto(id);
//			detalleCompra.setNombrepro(pro.getNombre());
//			detalleCompra.setCantidad(cantidad);
//			listaDetalleCompra.add(detalleCompra);
//		} catch (Exception e) {
//			JSFUtil.crearMensajeERROR(e.getMessage());
//		}
//	}
//
//	public String actionListenerFinalizarVenta() {
//		Date fecha = new Date();
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
//		String fechaActual = format.format(fecha);
//		if (listaDetalleCompra.size() > 0) {
//			try {
//				Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(fechaActual);
//				compra.setFechaFactura(date1);
//				compra.setNumFactura("");
//				CliCliente proveedor = managerCompra.findClienteByCedula(cedulaProveedorSeleccionado);
//				compra.setCliCliente(proveedor);
//				managerCompra.registrarCompra(compra, listaDetalleCompra);
//			} catch (Exception e) {
//				JSFUtil.crearMensajeERROR(e.getMessage());
//			}
//		} else {
//			JSFUtil.crearMensajeERROR("Lista de Compra Vacia.");
//			return "";
//		}
//		return "generar_factura";
//	}
//
//
//
	public void buscarPorCategoria() {
		listaProductos = mc.findProductosByCatId(idCatSeleccionada, nombreP);
		if (listaProductos.size() < 1) {
			JSFUtil.crearMensajeERROR("0 Productos Econtrados");
		} else {
			JSFUtil.crearMensajeINFO(listaProductos.size() + " Productos Econtrados");
		}
	}
//
//	public String actionListenerVerDetalles(String idCom) {
//		try {
//			FacFactura com = managerCompra.findVentaById(idCom);
//			compra = new FacFactura();
//			compra.setNumFactura(com.getNumFactura());
//			compra.setNombreEmpleado(com.getNombreEmpleado());
//			compra.setFechaFactura(com.getFechaFactura());
//			compra.setTotalFactura(com.getTotalFactura());
//			listaDetalleCompra = new ArrayList<DetalleVentaDTO>();
//			listaDetalleCompra = managerCompra.findDetalleVentasByComId(compra.getNumFactura());
//		} catch (Exception e) {
//			JSFUtil.crearMensajeERROR(e.getMessage());
//		}
//		return "generar_factura";
//	}
//
//	public void actionListenerEliminarProductoDetalleCompra(String idDetCom) {
//		listaDetalleCompra= managerCompra.eliminarProductoDetalleCompra(listaDetalleCompra, idDetCom);
//	}
//
//	public String actionCargarMenuComprar() {
////		// obtener la fecha de ayer:
////		fechaInicio = ModelUtil.addDays(new Date(), -1);
////		// obtener la fecha de hoy:
////		fechaFin = new Date();
////		listaBitacora = managerAuditoria.findBit
////		return proveedores;acoraByFecha(fechaInicio, fechaFin);
////		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaBitacora.size());
//		listaCategorias = managerCompra.findAllCategorias();
//		listaClientes = managerCompra.findAllCliClientes();
//		compra = new FacFactura();
//		listaProductos = new ArrayList<ProProducto>();
//		listaDetalleCompra= new ArrayList<DetalleVentaDTO>();
//		nombreP = "";
//		mensaje = "";
//		return "comprar";
//	}
//
//
//	public String actionCargarMenuCompra() {
//		// obtener la fecha de ayer:
//		fechaInicio = ModelUtil.addDays(new Date(), -1);
//		// obtener la fecha de hoy:
//		fechaFin = new Date();
//		listaCompras = managerCompra.findVentasByFecha(fechaInicio, fechaFin);
//		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaCompras.size());
//		return "compras";
//	}
//
//	public void actionListenerEliminarCompra(int idCom) {
//		try {
//			managerCompra.eliminarVenta(idCom);
//			JSFUtil.crearMensajeINFO("Compra Eliminada.");
//			listaCompras = managerCompra.findVentasByFecha(fechaInicio, fechaFin);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			JSFUtil.crearMensajeERROR(e.getMessage());
//		}
//	}
//	public void actionListenerBuscarCompras() {
//		listaCompras = managerCompra.findVentasByFecha(fechaInicio, fechaFin);
//	}
//
//	public ManagerVentas getManagerCompra() {
//		return managerCompra;
//	}
//
//	public void setManagerCompra(ManagerVentas managerCompra) {
//		this.managerCompra = managerCompra;
//	}
//
//	public List<FacFactura> getListaCompras() {
//		return listaCompras;
//	}
//
//	public void setListaCompras(List<FacFactura> listaCompras) {
//		this.listaCompras = listaCompras;
//	}
//
//	public List<CliCliente> getListaClientes() {
//		return listaClientes;
//	}
//
//	public void setListaClientes(List<CliCliente> listaClientes) {
//		this.listaClientes = listaClientes;
//	}
//
//	public List<DetalleVentaDTO> getListaDetalleCompra() {
//		return listaDetalleCompra;
//	}
//
//	public void setListaDetalleCompra(List<DetalleVentaDTO> listaDetalleCompra) {
//		this.listaDetalleCompra = listaDetalleCompra;
//	}
//
//	public List<TipoProducto> getListaCategorias() {
//		return listaCategorias;
//	}
//
//	public void setListaCategorias(List<TipoProducto> listaCategorias) {
//		this.listaCategorias = listaCategorias;
//	}
//
//	public List<ProProducto> getListaProductos() {
//		return listaProductos;
//	}
//
//	public void setListaProductos(List<ProProducto> listaProductos) {
//		this.listaProductos = listaProductos;
//	}
//
//	public String getCedulaProveedorSeleccionado() {
//		return cedulaProveedorSeleccionado;
//	}
//
//	public void setCedulaProveedorSeleccionado(String cedulaProveedorSeleccionado) {
//		this.cedulaProveedorSeleccionado = cedulaProveedorSeleccionado;
//	}
//
//	public int getIdCatSeleccionada() {
//		return idCatSeleccionada;
//	}
//
//	public void setIdCatSeleccionada(int idCatSeleccionada) {
//		this.idCatSeleccionada = idCatSeleccionada;
//	}
//
//	public int getIdProSeleccionado() {
//		return idProSeleccionado;
//	}
//
//	public void setIdProSeleccionado(int idProSeleccionado) {
//		this.idProSeleccionado = idProSeleccionado;
//	}
//
//	public int getCantidad() {
//		return cantidad;
//	}
//
//	public void setCantidad(int cantidad) {
//		this.cantidad = cantidad;
//	}
//
//	public String getMensaje() {
//		return mensaje;
//	}
//
//	public void setMensaje(String mensaje) {
//		this.mensaje = mensaje;
//	}
//
//	public FacFactura getCompra() {
//		return compra;
//	}
//
//	public void setCompra(FacFactura compra) {
//		this.compra = compra;
//	}
//
//	public String getNombreP() {
//		return nombreP;
//	}
//
//	public void setNombreP(String nombreP) {
//		this.nombreP = nombreP;
//	}
//
//	public Date getFechaInicio() {
//		return fechaInicio;
//	}
//
//	public void setFechaInicio(Date fechaInicio) {
//		this.fechaInicio = fechaInicio;
//	}
//
//	public Date getFechaFin() {
//		return fechaFin;
//	}
//
//	public void setFechaFin(Date fechaFin) {
//		this.fechaFin = fechaFin;
//	}
	
	public String actionReporte() {
	Map<String, Object> parametros = new HashMap<String, Object>();
	/*parametros.put("idCom", compra.getIdCompra());*/
	FacesContext context = FacesContext.getCurrentInstance();
	ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
	String ruta = servletContext.getRealPath("ventas/vender/reporte.jasper");
	System.out.println(ruta);
	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
	response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
	response.setContentType("application/pdf");
	try {
		Class.forName("org.postgresql.Driver");
		Connection connection = null;
		connection = DriverManager.getConnection("jdbc:postgresql://10.24.8.85:5432/epherrerag",
				"mipymes", "prueba21.");
		JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
		JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		context.getApplication().getStateManager().saveView(context);
		System.out.println("reporte generado.");
		context.responseComplete();
	} catch (Exception e) {
		JSFUtil.crearMensajeERROR(e.getMessage());
		e.printStackTrace();
	}
	return "";
}

	public ManagerVentas getMc() {
		return mc;
	}

	public void setMc(ManagerVentas mc) {
		this.mc = mc;
	}

	public List<FacFactura> getListaVentas() {
		return listaVentas;
	}

	public void setListaVentas(List<FacFactura> listaVentas) {
		this.listaVentas = listaVentas;
	}

	public List<DetalleVentaDTO> getListaDetVentas() {
		return listaDetVentas;
	}

	public void setListaDetVentas(List<DetalleVentaDTO> listaDetVentas) {
		this.listaDetVentas = listaDetVentas;
	}

	public List<CliCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<CliCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<TipoProducto> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<TipoProducto> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public List<ProProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public String getCedulaProveedorSeleccionado() {
		return cedulaProveedorSeleccionado;
	}

	public void setCedulaProveedorSeleccionado(String cedulaProveedorSeleccionado) {
		this.cedulaProveedorSeleccionado = cedulaProveedorSeleccionado;
	}

	public int getIdCatSeleccionada() {
		return idCatSeleccionada;
	}

	public void setIdCatSeleccionada(int idCatSeleccionada) {
		this.idCatSeleccionada = idCatSeleccionada;
	}

	public int getIdPro() {
		return idPro;
	}

	public void setIdPro(int idPro) {
		this.idPro = idPro;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getCedulacli() {
		return cedulacli;
	}

	public void setCedulacli(String cedulacli) {
		this.cedulacli = cedulacli;
	}

	public FacFactura getCompra() {
		return compra;
	}

	public void setCompra(FacFactura compra) {
		this.compra = compra;
	}

	public String getNombreP() {
		return nombreP;
	}

	public void setNombreP(String nombreP) {
		this.nombreP = nombreP;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<FormaDePago> getListaFormaDepago() {
		return listaFormaDepago;
	}

	public void setListaFormaDepago(List<FormaDePago> listaFormaDepago) {
		this.listaFormaDepago = listaFormaDepago;
	}



	public String getNombreempleado() {
		return nombreempleado;
	}

	public void setNombreempleado(String nombreempleado) {
		this.nombreempleado = nombreempleado;
	}

	public int getIdformadepago() {
		return idformadepago;
	}

	public void setIdformadepago(int idformadepago) {
		this.idformadepago = idformadepago;
	}

	public String getNumeroFactura() {
		return NumeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		NumeroFactura = numeroFactura;
	}

	public int getNumfactura() {
		return numfactura;
	}

	public void setNumfactura(int numfactura) {
		this.numfactura = numfactura;
	}



}
