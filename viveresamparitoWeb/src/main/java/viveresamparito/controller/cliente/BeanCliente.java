package viveresamparito.controller.cliente;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.CiuCiudad;
import minimarketdemo.model.core.entities.CliCliente;
import minimarketdemo.model.core.entities.TipoDeDocumento;
import viveresamparito.model.cliente.managers.ManagerCliente;





@Named
@SessionScoped
public class BeanCliente implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerCliente managerClientes;

	private int idclienteseleccionado;
	private Date fechaseleccionada;
	private int idtipodocumentoseleccionado;
	private int idciudadseleccionado;

	private List<CliCliente> listaclientes;
	private List<TipoDeDocumento> listaTipoDeDocumentos;
	private List<CiuCiudad> listaCiudades;
	private CliCliente cliente;
	private boolean panelColapsado;
	private CliCliente clienteseleccionado;
	

	@PostConstruct



	public void inicializar() {
		listaclientes = managerClientes.findAllCliClientes();
		listaTipoDeDocumentos = managerClientes.findAllTipoDeDocumentos();
		listaCiudades = managerClientes.findAllCiuCiudads();
		cliente = new CliCliente();
		panelColapsado = true;
	}
	public void actionListenerColapsarPanel() {
		panelColapsado=!panelColapsado;
	}

	public List<String> completeTextDocumentos(String query) {
		String queryLowerCase = query.toLowerCase();
		List<String> docList = new ArrayList<>();
		for (TipoDeDocumento doc : listaTipoDeDocumentos) {
			docList.add(doc.getDescripcion());
		}
		return docList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}

	public List<String> completeTextCiudades(String query) {
		String queryLowerCase = query.toLowerCase();
		List<String> ciuList = new ArrayList<>();
		for (CiuCiudad ciu : listaCiudades) {
			ciuList.add(ciu.getNombreCiudad());
		}
		return ciuList.stream().filter(t -> t.toLowerCase().startsWith(queryLowerCase)).collect(Collectors.toList());
	}


	
	public void actionListenerInsertarCliente() {
		
			managerClientes.insertarCliente(cliente,idtipodocumentoseleccionado,  idciudadseleccionado);
			listaclientes=managerClientes.findAllCliClientes();
			cliente=new CliCliente();
			JSFUtil.crearMensajeINFO("Datos del Cliente insertados");
		
	}
	
	

	public void actionListenerEliminarCliente(String documento) {
		managerClientes.eliminarCliente(documento);
		listaclientes = managerClientes.findAllCliClientes();
		JSFUtil.crearMensajeINFO("Cliente eliminado");

	}

	public void actionListenerSeleccionarCliente(CliCliente clientes) {
		clienteseleccionado = clientes ;

	}
	
	

	public void actionListenerActualizarCliente() {
		try {
			managerClientes.actualizarCliente(clienteseleccionado);
			listaclientes = managerClientes.findAllCliClientes();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

		
		
	}

	public ManagerCliente getManagerClientes() {
		return managerClientes;
	}

	public void setManagerClientes(ManagerCliente managerClientes) {
		this.managerClientes = managerClientes;
	}

	public int getIdclienteseleccionado() {
		return idclienteseleccionado;
	}

	public void setIdclienteseleccionado(int idclienteseleccionado) {
		this.idclienteseleccionado = idclienteseleccionado;
	}

	public Date getFechaseleccionada() {
		return fechaseleccionada;
	}

	public void setFechaseleccionada(Date fechaseleccionada) {
		this.fechaseleccionada = fechaseleccionada;
	}

	public int getIdtipodocumentoseleccionado() {
		return idtipodocumentoseleccionado;
	}

	public void setIdtipodocumentoseleccionado(int idtipodocumentoseleccionado) {
		this.idtipodocumentoseleccionado = idtipodocumentoseleccionado;
	}

	public int getIdciudadseleccionado() {
		return idciudadseleccionado;
	}

	public void setIdciudadseleccionado(int idciudadseleccionado) {
		this.idciudadseleccionado = idciudadseleccionado;
	}

	public List<CliCliente> getListaclientes() {
		return listaclientes;
	}

	public void setListaclientes(List<CliCliente> listaclientes) {
		this.listaclientes = listaclientes;
	}

	public List<TipoDeDocumento> getListaTipoDeDocumentos() {
		return listaTipoDeDocumentos;
	}

	public void setListaTipoDeDocumentos(List<TipoDeDocumento> listaTipoDeDocumentos) {
		this.listaTipoDeDocumentos = listaTipoDeDocumentos;
	}

	public List<CiuCiudad> getListaCiudades() {
		return listaCiudades;
	}

	public void setListaCiudades(List<CiuCiudad> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public CliCliente getCliente() {
		return cliente;
	}

	public void setCliente(CliCliente cliente) {
		this.cliente = cliente;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public CliCliente getClienteseleccionado() {
		return clienteseleccionado;
	}

	public void setClienteseleccionado(CliCliente clienteseleccionado) {
		this.clienteseleccionado = clienteseleccionado;
	}
	
	
	
	

	
	
	
	

}
