-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.1
-- PostgreSQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: new_database | type: DATABASE --
-- -- DROP DATABASE IF EXISTS new_database;
-- CREATE DATABASE new_database;
-- -- ddl-end --
-- 

-- object: public.pro_producto | type: TABLE --
-- DROP TABLE IF EXISTS public.pro_producto CASCADE;
CREATE TABLE public.pro_producto(
	id_producto integer NOT NULL,
	descripcion varchar(50) NOT NULL,
	nombre varchar(50) NOT NULL,
	precio_costo decimal(11,2) NOT NULL,
	precio_venta decimal(11,2) NOT NULL,
	stock integer NOT NULL,
	fecha_ingreso date NOT NULL,
	cod_tipo_producto integer NOT NULL,
	cod_proveedor varchar(20) NOT NULL,
	CONSTRAINT pro_producto_pk PRIMARY KEY (id_producto)

);
-- ddl-end --
ALTER TABLE public.pro_producto OWNER TO postgres;
-- ddl-end --

-- object: public.cli_cliente | type: TABLE --
-- DROP TABLE IF EXISTS public.cli_cliente CASCADE;
CREATE TABLE public.cli_cliente(
	cedula_cliente varchar(15) NOT NULL,
	nombres varchar(50) NOT NULL,
	apellidos varchar(50) NOT NULL,
	direccion varchar(50),
	telefono varchar(50),
	cod_tipo_documento integer NOT NULL,
	cod_ciudad integer NOT NULL,
	CONSTRAINT cli_cliente_pk PRIMARY KEY (cedula_cliente)

);
-- ddl-end --
ALTER TABLE public.cli_cliente OWNER TO postgres;
-- ddl-end --

-- object: public.pro_proveedor | type: TABLE --
-- DROP TABLE IF EXISTS public.pro_proveedor CASCADE;
CREATE TABLE public.pro_proveedor(
	cedula_proveedor varchar(50) NOT NULL,
	nombres varchar(50) NOT NULL,
	apellidos varchar(50) NOT NULL,
	nombre_comercial varchar(50) NOT NULL,
	direccion varchar(50),
	telefono varchar(50),
	cod_tipo_documento integer NOT NULL,
	cod_ciudad integer NOT NULL,
	CONSTRAINT pro_proveedor_pk PRIMARY KEY (cedula_proveedor)

);
-- ddl-end --
ALTER TABLE public.pro_proveedor OWNER TO postgres;
-- ddl-end --

-- object: public.fac_factura | type: TABLE --
-- DROP TABLE IF EXISTS public.fac_factura CASCADE;
CREATE TABLE public.fac_factura(
	num_factura varchar(50) NOT NULL,
	nombre_empleado varchar(50) NOT NULL,
	fecha_factura date NOT NULL,
	total_factura decimal(11,2),
	cod_cliente varchar(15) NOT NULL,
	cod_formapago integer NOT NULL,
	CONSTRAINT fac_factura_pk PRIMARY KEY (num_factura)

);
-- ddl-end --
ALTER TABLE public.fac_factura OWNER TO postgres;
-- ddl-end --

-- object: public.det_factura | type: TABLE --
-- DROP TABLE IF EXISTS public.det_factura CASCADE;
CREATE TABLE public.det_factura(
	id_det_dactura serial NOT NULL,
	cantidad integer NOT NULL,
	total decimal(11,2) NOT NULL,
	id_detfac_factura varchar,
	id_detfac_producto integer,
	CONSTRAINT det_factura_pk PRIMARY KEY (id_det_dactura)

);
-- ddl-end --
ALTER TABLE public.det_factura OWNER TO postgres;
-- ddl-end --

-- object: public.tipo_de_documento | type: TABLE --
-- DROP TABLE IF EXISTS public.tipo_de_documento CASCADE;
CREATE TABLE public.tipo_de_documento(
	id_tipo_documento integer NOT NULL,
	descripcion varchar(10) NOT NULL,
	CONSTRAINT tipo_de_documento_pk PRIMARY KEY (id_tipo_documento)

);
-- ddl-end --
ALTER TABLE public.tipo_de_documento OWNER TO postgres;
-- ddl-end --

-- object: public.ciu_ciudad | type: TABLE --
-- DROP TABLE IF EXISTS public.ciu_ciudad CASCADE;
CREATE TABLE public.ciu_ciudad(
	codigo_ciudad integer NOT NULL,
	nombre_ciudad varchar(30) NOT NULL,
	CONSTRAINT ciu_ciudad_pk PRIMARY KEY (codigo_ciudad)

);
-- ddl-end --
ALTER TABLE public.ciu_ciudad OWNER TO postgres;
-- ddl-end --

-- object: public.tipo_producto | type: TABLE --
-- DROP TABLE IF EXISTS public.tipo_producto CASCADE;
CREATE TABLE public.tipo_producto(
	id_tipo_producto integer NOT NULL,
	descripcion varchar(50) NOT NULL,
	CONSTRAINT tipo_articulo_pk PRIMARY KEY (id_tipo_producto)

);
-- ddl-end --
ALTER TABLE public.tipo_producto OWNER TO postgres;
-- ddl-end --

-- object: public.forma_de_pago | type: TABLE --
-- DROP TABLE IF EXISTS public.forma_de_pago CASCADE;
CREATE TABLE public.forma_de_pago(
	id_forma_pago integer NOT NULL,
	descripcion varchar(50) NOT NULL,
	CONSTRAINT forma_de_pago_pk PRIMARY KEY (id_forma_pago)

);
-- ddl-end --
ALTER TABLE public.forma_de_pago OWNER TO postgres;
-- ddl-end --

-- object: public.com_compra | type: TABLE --
-- DROP TABLE IF EXISTS public.com_compra CASCADE;
CREATE TABLE public.com_compra(
	id_compra integer NOT NULL,
	fecha date NOT NULL,
	numero_compra integer NOT NULL,
	id_compra_proveedor varchar NOT NULL,
	CONSTRAINT com_compra_pk PRIMARY KEY (id_compra)

);
-- ddl-end --
ALTER TABLE public.com_compra OWNER TO postgres;
-- ddl-end --

-- object: public.det_compra | type: TABLE --
-- DROP TABLE IF EXISTS public.det_compra CASCADE;
CREATE TABLE public.det_compra(
	cantidad decimal(11,2) NOT NULL,
	id_detcom_compra integer NOT NULL,
	id_detpro_producto integer NOT NULL,
	id_det_compra serial NOT NULL,
	CONSTRAINT det_compra_pk PRIMARY KEY (id_det_compra)

);
-- ddl-end --
ALTER TABLE public.det_compra OWNER TO postgres;
-- ddl-end --

-- object: fk_cod_tipo_producto | type: CONSTRAINT --
-- ALTER TABLE public.pro_producto DROP CONSTRAINT IF EXISTS fk_cod_tipo_producto CASCADE;
ALTER TABLE public.pro_producto ADD CONSTRAINT fk_cod_tipo_producto FOREIGN KEY (cod_tipo_producto)
REFERENCES public.tipo_producto (id_tipo_producto) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_proveedor | type: CONSTRAINT --
-- ALTER TABLE public.pro_producto DROP CONSTRAINT IF EXISTS fk_cod_proveedor CASCADE;
ALTER TABLE public.pro_producto ADD CONSTRAINT fk_cod_proveedor FOREIGN KEY (cod_proveedor)
REFERENCES public.pro_proveedor (cedula_proveedor) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_tipo_documento | type: CONSTRAINT --
-- ALTER TABLE public.cli_cliente DROP CONSTRAINT IF EXISTS fk_cod_tipo_documento CASCADE;
ALTER TABLE public.cli_cliente ADD CONSTRAINT fk_cod_tipo_documento FOREIGN KEY (cod_tipo_documento)
REFERENCES public.tipo_de_documento (id_tipo_documento) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_ciudad | type: CONSTRAINT --
-- ALTER TABLE public.cli_cliente DROP CONSTRAINT IF EXISTS fk_cod_ciudad CASCADE;
ALTER TABLE public.cli_cliente ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad)
REFERENCES public.ciu_ciudad (codigo_ciudad) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_tipo_documento | type: CONSTRAINT --
-- ALTER TABLE public.pro_proveedor DROP CONSTRAINT IF EXISTS fk_cod_tipo_documento CASCADE;
ALTER TABLE public.pro_proveedor ADD CONSTRAINT fk_cod_tipo_documento FOREIGN KEY (cod_tipo_documento)
REFERENCES public.tipo_de_documento (id_tipo_documento) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_ciudad | type: CONSTRAINT --
-- ALTER TABLE public.pro_proveedor DROP CONSTRAINT IF EXISTS fk_cod_ciudad CASCADE;
ALTER TABLE public.pro_proveedor ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad)
REFERENCES public.ciu_ciudad (codigo_ciudad) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_cliente | type: CONSTRAINT --
-- ALTER TABLE public.fac_factura DROP CONSTRAINT IF EXISTS fk_cod_cliente CASCADE;
ALTER TABLE public.fac_factura ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente)
REFERENCES public.cli_cliente (cedula_cliente) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_cod_formapago | type: CONSTRAINT --
-- ALTER TABLE public.fac_factura DROP CONSTRAINT IF EXISTS fk_cod_formapago CASCADE;
ALTER TABLE public.fac_factura ADD CONSTRAINT fk_cod_formapago FOREIGN KEY (cod_formapago)
REFERENCES public.forma_de_pago (id_forma_pago) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_id_detfac_producto | type: CONSTRAINT --
-- ALTER TABLE public.det_factura DROP CONSTRAINT IF EXISTS fk_id_detfac_producto CASCADE;
ALTER TABLE public.det_factura ADD CONSTRAINT fk_id_detfac_producto FOREIGN KEY (id_detfac_producto)
REFERENCES public.pro_producto (id_producto) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_detfac_factura | type: CONSTRAINT --
-- ALTER TABLE public.det_factura DROP CONSTRAINT IF EXISTS fk_detfac_factura CASCADE;
ALTER TABLE public.det_factura ADD CONSTRAINT fk_detfac_factura FOREIGN KEY (id_detfac_factura)
REFERENCES public.fac_factura (num_factura) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_compra_proveedor | type: CONSTRAINT --
-- ALTER TABLE public.com_compra DROP CONSTRAINT IF EXISTS fk_compra_proveedor CASCADE;
ALTER TABLE public.com_compra ADD CONSTRAINT fk_compra_proveedor FOREIGN KEY (id_compra_proveedor)
REFERENCES public.pro_proveedor (cedula_proveedor) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_detcom_compra | type: CONSTRAINT --
-- ALTER TABLE public.det_compra DROP CONSTRAINT IF EXISTS fk_detcom_compra CASCADE;
ALTER TABLE public.det_compra ADD CONSTRAINT fk_detcom_compra FOREIGN KEY (id_detcom_compra)
REFERENCES public.com_compra (id_compra) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_detpro_producto | type: CONSTRAINT --
-- ALTER TABLE public.det_compra DROP CONSTRAINT IF EXISTS fk_detpro_producto CASCADE;
ALTER TABLE public.det_compra ADD CONSTRAINT fk_detpro_producto FOREIGN KEY (id_detpro_producto)
REFERENCES public.pro_producto (id_producto) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --





