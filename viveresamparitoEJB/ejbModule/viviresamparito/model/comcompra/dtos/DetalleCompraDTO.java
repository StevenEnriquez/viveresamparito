package viviresamparito.model.comcompra.dtos;

public class DetalleCompraDTO {
	private int idDetCom;
	private int idPro;
	private String nombrePro;
	private double cantidad;
	
	public DetalleCompraDTO(int idDetCom, int idPro, String nombrePro, double cantidad) {
		super();
		this.idDetCom = idDetCom;
		this.idPro = idPro;
		this.nombrePro = nombrePro;
		this.cantidad = cantidad;
	}
	public DetalleCompraDTO() {
		super();
		this.idDetCom = 0;
		this.idPro = 0;
		this.nombrePro = "";
		this.cantidad = 0;
	}
	public int getIdDetCom() {
		return idDetCom;
	}
	public void setIdDetCom(int idDetCom) {
		this.idDetCom = idDetCom;
	}
	public int getIdPro() {
		return idPro;
	}
	public void setIdPro(int idPro) {
		this.idPro = idPro;
	}
	public String getNombrePro() {
		return nombrePro;
	}
	public void setNombrePro(String nombrePro) {
		this.nombrePro = nombrePro;
	}
	public double getCantidad() {
		return cantidad;
	}
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

}
