package viviresamparito.model.comcompra.dtos;

public class ProveedorDTO {
	private int id;
	private String cedulaProveedor;
	private String nombreComercial;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCedulaProveedor() {
		return cedulaProveedor;
	}
	public void setCedulaProveedor(String cedulaProveedor) {
		this.cedulaProveedor = cedulaProveedor;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	
}
