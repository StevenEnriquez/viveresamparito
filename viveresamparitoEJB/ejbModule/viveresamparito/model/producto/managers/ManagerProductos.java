package viveresamparito.model.producto.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.ProProducto;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoProducto;

/**
 * Session Bean implementation class ManagerProductos
 */
@Stateless
@LocalBean
public class ManagerProductos {
	@PersistenceContext
	private EntityManager em;
		/**
		 * Default constructor.
		 */
		public ManagerProductos() {
			// TODO Auto-generated constructor stub
		}
		
		
		public List<TipoProducto> findAllTipoProductos(){
	    	TypedQuery<TipoProducto> q = em.createQuery("select o from TipoProducto o order by o.idTipoProducto", TipoProducto.class);
	    	return q.getResultList();
	    }
	    
	    public List<ProProducto> findAllProProductos(){
	    	TypedQuery<ProProducto> q = em.createQuery("select o from ProProducto o order by o.descripcion ", ProProducto.class);
	    	return q.getResultList();
	    }
	    public List<ProProveedor> findAllProProveedores(){
	    	TypedQuery<ProProveedor> q = em.createQuery("select o from ProProveedor o order by o.cedulaProveedor", ProProveedor.class);
	    	return q.getResultList();
	    }
		
	    public ProProducto findProductosByNombre(int descripcion) {
			return em.find(ProProducto.class, descripcion);
		}
	    
	    public ProProducto findProductosByDescripcion(String descripcion) {
			return em.find(ProProducto.class, descripcion);
		}
	    
	    
		public void  insertarProductos(ProProducto descripcion, int idtipoproductoseleccionado, String idproveedorseleccionado) {
			TipoProducto tipoProducto = em.find(TipoProducto.class, idtipoproductoseleccionado);
			ProProveedor proProveedor = em.find(ProProveedor.class, idproveedorseleccionado);
			if (findProductosByNombre(descripcion.getIdProducto()) != null)
				System.out.println("Ya existe el producto indicado.");
				descripcion.setProProveedor(proProveedor);
				descripcion.setTipoProducto(tipoProducto);
			em.persist(descripcion);
		}
		 public void actualizarProductos(ProProducto descripcion) throws Exception {
		        ProProducto e = findProductosByDescripcion(descripcion.getDescripcion());
		        if (e == null)
					throw new Exception("No existe el documento con el id especificado.");
				e.setDescripcion(descripcion.getDescripcion());
				e.setIdProducto(descripcion.getIdProducto());
				e.setNombre(descripcion.getNombre());
				e.setPrecioCosto(descripcion.getPrecioCosto());
				e.setPrecioVenta(descripcion.getPrecioVenta());
				e.setFechaIngreso(descripcion.getFechaIngreso());
				e.setStock(descripcion.getStock());
				
				em.merge(e);

		    }

		public void eliminarProducto(int descripcion) {
			ProProducto producto = findProductosByNombre(descripcion);
			if (producto != null)
				em.remove(producto);
		}
	
	}	

