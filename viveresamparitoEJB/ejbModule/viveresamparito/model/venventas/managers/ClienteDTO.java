package viveresamparito.model.venventas.managers;

public class ClienteDTO {
	private String cedulaCliente;

	private String apellidos;

	private String direccion;

	private String nombres;

	private String telefono;
	
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ClienteDTO() {
		this.cedulaCliente ="";
		this.nombres = "";
	}
	public ClienteDTO(String cedulacliente, String nombres) {
		this.cedulaCliente = cedulacliente;
		this.nombres = nombres;
	}

	public String getCedulaCliente() {
		return cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
}
