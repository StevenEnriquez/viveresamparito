package viveresamparito.model.venventas.managers;

import java.math.BigDecimal;

public class DetalleVentaDTO {

	private int cantidad;
	private int idProducto;
	private String nombrepro;
	private String idFactura;
	private BigDecimal subtotal;
	private double precioUnitario;
	
	public String getNombrepro() {
		return nombrepro;
	}
	public void setNombrepro(String nombrepro) {
		this.nombrepro = nombrepro;
	}
	
//	public DetalleVentaDTO() {
//		this.cantidad = 0;
//		this.total = 0;
//		this.idProducto = 0;
//		this.idFactura = "";
//		this.nombrepro="";
//
//	}
//	public DetalleVentaDTO(double cantidad, double total, int idProducto, String idFactura, String nombrepro) {
//		super();
//		this.cantidad = cantidad;
//		this.total = total;
//		this.idProducto = idProducto;
//		this.idFactura = idFactura;
//		this.nombrepro=nombrepro;
//
//	}
	
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public int getCantidad() {
		return cantidad;
	}

	public double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}
	

}
