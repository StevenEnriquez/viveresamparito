package viveresamparito.model.venventas.managers;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CiuCiudad;
import minimarketdemo.model.core.entities.CliCliente;
import minimarketdemo.model.core.entities.ComCompra;
import minimarketdemo.model.core.entities.DetCompra;
import minimarketdemo.model.core.entities.DetFactura;
import minimarketdemo.model.core.entities.FacFactura;
import minimarketdemo.model.core.entities.FormaDePago;
import minimarketdemo.model.core.entities.ProProducto;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoProducto;
import minimarketdemo.model.core.managers.ManagerDAO;
import viviresamparito.model.comcompra.dtos.DetalleCompraDTO;

/**
 * Session Bean implementation class ManagerVentas
 */
@Stateless
@LocalBean
public class ManagerVentas {
	private ManagerDAO mDAO;
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerVentas() {
		// TODO Auto-generated constructor stub
	}

	public CliCliente findClienteByCedula(String cedula) throws Exception {
		CliCliente cliente = new CliCliente();

		try {
			cliente = (CliCliente) mDAO.findById(CliCliente.class, cedula);

		} catch (Exception e) {
			throw new Exception("Proveedor No Registrado");
		}
		return cliente;
	}

	public List<CliCliente> findAllCliClientes() {
		TypedQuery<CliCliente> q = em.createQuery("select p from CliCliente p order by p.cedulaCliente",
				CliCliente.class);
		return q.getResultList();
	}


	public List<FormaDePago> findAllFormaDePagos() {
		TypedQuery<FormaDePago> q = em.createQuery("select p from FormaDePago p order by p.idFormaPago",
				FormaDePago.class);
		return q.getResultList();
	}

	public List<ProProducto> findAllVtnProducto() {
		TypedQuery<ProProducto> q = em.createQuery("select p from ProProducto p order by p.descripcion",
				ProProducto.class);
		return q.getResultList();
	}
	
	public List<TipoProducto> findAllTipoProductos() {
		TypedQuery<TipoProducto> q = em.createQuery("select p from TipoProducto p order by p.idTipoProducto",
				TipoProducto.class);
		return q.getResultList();
	}

//	public List<CliCliente> findAllCliClientes() {
//		List<CliCliente> clientes = new ArrayList<CliCliente>();
//		clientes = mDAO.findAll(CliCliente.class);

//		List<ProveedorDTO> listaProveedores = new ArrayList<ProveedorDTO>();
//		int cont = 0;
//		for (ProProveedor proveedor : proveedores) {
//			ProveedorDTO proveedorDTO = new ProveedorDTO();
//			proveedorDTO.setId(cont);
//			proveedorDTO.setNombreComercial(proveedor.getNombreComercial());
//			proveedorDTO.setCedulaProveedor(proveedor.getCedulaProveedor());
//			listaProveedores.add(proveedorDTO);
//			cont++;
//		}

//		return clientes;
//	}

	public DetalleVentaDTO crearDetalleVenta(int idProducto, int cantidad) {
		ProProducto a = em.find(ProProducto.class, idProducto);
//		a.setCantidad(cantidad);
		DetalleVentaDTO detalle = new DetalleVentaDTO();
		detalle.setIdProducto(idProducto);
		detalle.setNombrepro(a.getNombre());
		detalle.setCantidad(cantidad);
		detalle.setPrecioUnitario(a.getPrecioVenta().doubleValue());
		double x = cantidad * a.getPrecioVenta().doubleValue();
		detalle.setSubtotal(new BigDecimal(x));
		return detalle;
	}

	public double totalDetalleCompra(List<DetalleVentaDTO> listaDetalleVenta) {
		double total = 0;
		for (DetalleVentaDTO d : listaDetalleVenta)
			total += d.getSubtotal().doubleValue();
		return total;
	}

	public void productosMultiples(List<DetalleVentaDTO> listaDetalleVentas, String cedula, String NombreEmpleado, int idFormPago) {
		CliCliente cliente = em.find(CliCliente.class, cedula);
		FormaDePago formapago = em.find(FormaDePago.class, idFormPago);
		FacFactura ccab = new FacFactura();
		ccab.setFormaDePago(formapago);
		ccab.setNombreEmpleado(NombreEmpleado);
		ccab.setCliCliente(cliente);
		ccab.setFechaFactura(new Date());
		ccab.setTotalFactura(new BigDecimal (totalDetalleCompra(listaDetalleVentas)));
		List<DetFactura> listaDetalle = new ArrayList<DetFactura>();
		ccab.setDetFacturas(listaDetalle);
		for (DetalleVentaDTO det : listaDetalleVentas) {
			DetFactura comDet = new DetFactura();
			comDet.setFacFactura(ccab);
			ProProducto produ = em.find(ProProducto.class, det.getIdProducto());

			comDet.setProProducto(produ);
			comDet.setCantidad(det.getCantidad());
			comDet.setTotal(det.getSubtotal());
			
			listaDetalle.add(comDet);
		}
		em.persist(ccab);
	}

//	public List<TipoProducto> findAllCategorias() {
//		List<TipoProducto> listaCategorias = new ArrayList<TipoProducto>();
//		listaCategorias = mDAO.findAll(TipoProducto.class);
//		TipoProducto categoria = new TipoProducto();
//		categoria.setIdTipoProducto(0);
//		categoria.setDescripcion("Todas");
//		listaCategorias.add(categoria);
//		return listaCategorias;
//	}
//
//	public FacFactura findVentaById(String idFactura) throws Exception {
//		FacFactura factura = (FacFactura) mDAO.findById(FacFactura.class, idFactura);
//		if (factura == null) {
//			throw new Exception("Compra no encontrada.");
//		}
//		return factura;
//	}
//
//	public List<FacFactura> findVentasByFecha(Date fechaInicio, Date fechaFin) {
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//		String consulta = "select c from ComCompra c where c.fecha between :fechaInicio and :fechaFin order by c.fecha";
//		Query q = mDAO.getEntityManager().createQuery(consulta, FacFactura.class);
//		q.setParameter("fechaInicio", new Timestamp(fechaInicio.getTime()));
//		q.setParameter("fechaFin", new Timestamp(fechaFin.getTime()));
//		return q.getResultList();
//	}
//
//	public List<DetalleVentaDTO> findDetalleVentasByComId(String idCom) {
//		String clausula = "id_detcom_compra = " + idCom;
//		List<DetFactura> listaDetalleCompras = mDAO.findWhere(DetFactura.class, clausula, "");
//		List<DetalleVentaDTO> lista = new ArrayList<DetalleVentaDTO>();
//		int i = 1;
//		for (DetFactura detCom : listaDetalleCompras) {
//			DetalleVentaDTO detComDTO = new DetalleVentaDTO();
////			detComDTO.setIdDetCom(i);
//			detComDTO.setIdProducto(i);
////			detComDTO.setIdProducto(detCom.getProProducto().getIdProducto());
//			detComDTO.setNombrepro(detCom.getProProducto().getNombre());
//			detComDTO.setCantidad(detCom.getCantidad().doubleValue());
//			detComDTO.setTotal(detCom.getTotal().doubleValue());
//			lista.add(detComDTO);
//			i++;
//		}
//		return lista;
//	}
//
	public List<ProProducto> findProductosByCatId(int idCat, String nombreP) {
		List<ProProducto> listaProductos = new ArrayList<ProProducto>();
		String clausula = "";
		if (idCat == 0) {
			if (nombreP.length() > 0) {
				clausula = "nombre LIKE '" + nombreP + "%' or nombre LIKE '%" + nombreP + "%' or nombre LIKE '%"
						+ nombreP + "'";
				listaProductos = mDAO.findWhere(ProProducto.class, clausula, "");
				return listaProductos;
			} else {
				listaProductos = mDAO.findAll(ProProducto.class);
				return listaProductos;
			}
		}
		if (nombreP.length() > 0) {
			clausula = "nombre LIKE '" + nombreP + "%' or nombre LIKE '%" + nombreP + "%' or nombre LIKE '%" + nombreP
					+ "' and id_tipo_producto = '" + idCat + "'";
			listaProductos = mDAO.findWhere(ProProducto.class, clausula, "");
		} else {
			clausula = "id_tipo_producto = '" + idCat + "'";
			listaProductos = mDAO.findWhere(ProProducto.class, clausula, "");
		}
		return listaProductos;
	}
//
//	public ProProducto findProductoById(int idPro) throws Exception {
//		ProProducto pro = (ProProducto) mDAO.findById(ProProducto.class, idPro);
//		if (pro == null) {
//			throw new Exception("Seleccione Un Producto.");
//		}
//		return pro;
//	}
//
////	public UsuarioDTO findUsuarioById(int idUsu) throws Exception {
////		SegUsuario usu = (SegUsuario) mDAO.findById(SegUsuario.class, idUsu);
////		if (usu == null) {
////			throw new Exception("Usuario no encontrado.");
////		}
////		UsuarioDTO usuarioDTO = new UsuarioDTO();
////		usuarioDTO.setIdUsu(usu.getIdSegUsuario());
////		usuarioDTO.setNombres(usu.getNombres() + " " + usu.getApellidos());
////		return usuarioDTO;
////	}
//
//	public FacFactura registrarCompra(FacFactura compra, List<DetalleVentaDTO> listaDetalleCompra) throws Exception {
//		mDAO.insertar(compra);
//		for (DetalleVentaDTO detCom : listaDetalleCompra) {
//			DetFactura detalleCompra = new DetFactura();
//			detalleCompra.setFacFactura(compra);
//			ProProducto producto = (ProProducto) mDAO.findById(ProProducto.class, detCom.getIdProducto());
//			detalleCompra.setProProducto(producto);
//			detalleCompra.setTotal(new BigDecimal(detCom.getTotal()));
//			mDAO.insertar(detalleCompra);
//		}
//		return compra;
//	}
//
//	public List<DetalleVentaDTO> eliminarProductoDetalleCompra(List<DetalleVentaDTO> lista, String idDetCom) {
//		if (lista == null)
//			return null;
//		int i = 0;
//		for (DetalleVentaDTO detCom : lista) {
//			if (detCom.getIdFactura() == idDetCom) {
//				lista.remove(i);
//				break;
//			}
//			i++;
//		}
//		return lista;
//	}
//
//	public void eliminarVenta(int idCompra) throws Exception {
//		mDAO.eliminar(FacFactura.class, idCompra);
//	}
}
