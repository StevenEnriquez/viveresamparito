package viveresamparito.model.ciudad.managers;




import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.CiuCiudad;


/**
 * Session Bean implementation class ManagerEstudiante
 */
@Stateless
@LocalBean
public class ManagerCiudad {
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public ManagerCiudad() {
        // TODO Auto-generated constructor stub
    }
    public List<CiuCiudad> findAllCiudad(){
    	String consulta="select o from CiuCiudad o order by o.codigoCiudad ASC";
    	Query q=em.createQuery(consulta, CiuCiudad.class);
    	return q.getResultList();
    }
    
    public CiuCiudad findCiuCiudadByID(int codigoCiudad) {
    	return em.find(CiuCiudad.class, codigoCiudad);
    	
    }
    
    public void insertarCiudad(CiuCiudad codigoCiudad) throws Exception {
    	if(findCiuCiudadByID(codigoCiudad.getCodigoCiudad())!=null)
    		throw new Exception("Ya existe la ciudad indicada");
    	em.persist(codigoCiudad);
    }
    
    public void eliminarCiudad(int codigoCiudad) {
    	CiuCiudad ciudad=findCiuCiudadByID(codigoCiudad);
    	if(ciudad!=null)
    		em.remove(ciudad);
    	
    }
    public void actualizarCiudad(CiuCiudad ciudad) throws Exception {
        CiuCiudad e = findCiuCiudadByID(ciudad.getCodigoCiudad());
        if (e == null)
            throw new Exception("No existe la ciudad especificada.");
        e.setNombreCiudad(ciudad.getNombreCiudad());
        em.merge(e);

    }

}

