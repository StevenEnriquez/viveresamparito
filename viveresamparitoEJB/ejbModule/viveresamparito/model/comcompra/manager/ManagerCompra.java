package viveresamparito.model.comcompra.manager;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.ComCompra;
import minimarketdemo.model.core.entities.DetCompra;
import minimarketdemo.model.core.entities.ProProducto;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoProducto;
import minimarketdemo.model.core.managers.ManagerDAO;
import viviresamparito.model.comcompra.dtos.DetalleCompraDTO;
import viviresamparito.model.comcompra.dtos.ProveedorDTO;

/**
 * Session Bean implementation class ManagerCompra
 */
@Stateless
@LocalBean
public class ManagerCompra {
	@EJB
	private ManagerDAO mDAO;

	/**
	 * Default constructor.
	 */
	public ManagerCompra() {
		// TODO Auto-generated constructor stub
	}

	public ProProveedor findProveedorByCedula(String cedula) throws Exception {
		ProProveedor proveedor = new ProProveedor();

		try {
			proveedor = (ProProveedor) mDAO.findById(ProProveedor.class, cedula);

		} catch (Exception e) {
			throw new Exception("Proveedor No Registrado");
		}
		return proveedor;
	}

	public List<ProProveedor> findAllProveedores() {
		List<ProProveedor> proveedores = new ArrayList<ProProveedor>();
		proveedores = mDAO.findAll(ProProveedor.class);

//		List<ProveedorDTO> listaProveedores = new ArrayList<ProveedorDTO>();
//		int cont = 0;
//		for (ProProveedor proveedor : proveedores) {
//			ProveedorDTO proveedorDTO = new ProveedorDTO();
//			proveedorDTO.setId(cont);
//			proveedorDTO.setNombreComercial(proveedor.getNombreComercial());
//			proveedorDTO.setCedulaProveedor(proveedor.getCedulaProveedor());
//			listaProveedores.add(proveedorDTO);
//			cont++;
//		}
		
		return proveedores;
	}

	public List<TipoProducto> findAllCategorias() {
		List<TipoProducto> listaCategorias = new ArrayList<TipoProducto>();
		listaCategorias = mDAO.findAll(TipoProducto.class);
		TipoProducto categoria = new TipoProducto();
		categoria.setIdTipoProducto(0);
		categoria.setDescripcion("Todas");
		listaCategorias.add(categoria);
		return listaCategorias;
	}

	public ComCompra findCompraById(int idCompra) throws Exception {
		ComCompra compra = (ComCompra) mDAO.findById(ComCompra.class, idCompra);
		if (compra == null) {
			throw new Exception("Compra no encontrada.");
		}
		return compra;
	}

	public List<ComCompra> findComprasByFecha(Date fechaInicio, Date fechaFin) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String consulta = "select c from ComCompra c where c.fecha between :fechaInicio and :fechaFin order by c.fecha";
		Query q = mDAO.getEntityManager().createQuery(consulta, ComCompra.class);
		q.setParameter("fechaInicio", new Timestamp(fechaInicio.getTime()));
		q.setParameter("fechaFin", new Timestamp(fechaFin.getTime()));
		return q.getResultList();
	}

	public List<DetalleCompraDTO> findDetalleComprasByComId(int idCom) {
		String clausula = "id_detcom_compra = " + idCom;
		List<DetCompra> listaDetalleCompras = mDAO.findWhere(DetCompra.class, clausula, "");
		List<DetalleCompraDTO> lista = new ArrayList<DetalleCompraDTO>();
		int i = 1;
		for (DetCompra detCom : listaDetalleCompras) {
			DetalleCompraDTO detComDTO = new DetalleCompraDTO();
			detComDTO.setIdDetCom(i);
			detComDTO.setIdPro(detCom.getProProducto().getIdProducto());
			detComDTO.setNombrePro(detCom.getProProducto().getNombre());
			detComDTO.setCantidad(detCom.getCantidad().doubleValue());
			lista.add(detComDTO);
			i++;
		}
		return lista;
	}

	public List<ProProducto> findProductosByCatId(int idCat, String nombreP) {
		List<ProProducto> listaProductos = new ArrayList<ProProducto>();
		String clausula = "";
		if (idCat == 0) {
			if (nombreP.length() > 0) {
				clausula = "nombre LIKE '" + nombreP + "%' or nombre LIKE '%" + nombreP + "%' or nombre LIKE '%"
						+ nombreP + "'";
				listaProductos = mDAO.findWhere(ProProducto.class, clausula, "");
				return listaProductos;
			} else {
				listaProductos = mDAO.findAll(ProProducto.class);
				return listaProductos;
			}
		}
		if (nombreP.length() > 0) {
			clausula = "nombre LIKE '" + nombreP + "%' or nombre LIKE '%" + nombreP + "%' or nombre LIKE '%" + nombreP
					+ "' and id_tipo_producto = '" + idCat + "'";
			listaProductos = mDAO.findWhere(ProProducto.class, clausula, "");
		} else {
			clausula = "id_tipo_producto = '" + idCat + "'";
			listaProductos = mDAO.findWhere(ProProducto.class, clausula, "");
		}
		return listaProductos;
	}

	public ProProducto findProductoById(int idPro) throws Exception {
		ProProducto pro = (ProProducto) mDAO.findById(ProProducto.class, idPro);
		if (pro == null) {
			throw new Exception("Seleccione Un Producto.");
		}
		return pro;
	}

//	public UsuarioDTO findUsuarioById(int idUsu) throws Exception {
//		SegUsuario usu = (SegUsuario) mDAO.findById(SegUsuario.class, idUsu);
//		if (usu == null) {
//			throw new Exception("Usuario no encontrado.");
//		}
//		UsuarioDTO usuarioDTO = new UsuarioDTO();
//		usuarioDTO.setIdUsu(usu.getIdSegUsuario());
//		usuarioDTO.setNombres(usu.getNombres() + " " + usu.getApellidos());
//		return usuarioDTO;
//	}

	public ComCompra registrarCompra(ComCompra compra, List<DetalleCompraDTO> listaDetalleCompra) throws Exception {
		mDAO.insertar(compra);
		for (DetalleCompraDTO detCom : listaDetalleCompra) {
			DetCompra detalleCompra = new DetCompra();
			detalleCompra.setComCompra(compra);
			ProProducto producto = (ProProducto) mDAO.findById(ProProducto.class, detCom.getIdPro());
			detalleCompra.setProProducto(producto);
			detalleCompra.setCantidad(new BigDecimal(detCom.getCantidad()));
			mDAO.insertar(detalleCompra);
		}
		return compra;
	}

	public List<DetalleCompraDTO> eliminarProductoDetalleCompra(List<DetalleCompraDTO> lista, int idDetCom) {
		if (lista == null)
			return null;
		int i = 0;
		for (DetalleCompraDTO detCom : lista) {
			if (detCom.getIdDetCom() == idDetCom) {
				lista.remove(i);
				break;
			}
			i++;
		}
		return lista;
	}

	public void eliminarCompra(int idCompra) throws Exception {
		mDAO.eliminar(ComCompra.class, idCompra);
	}
	// Verificar eliminacion de los detalles de compra

}

