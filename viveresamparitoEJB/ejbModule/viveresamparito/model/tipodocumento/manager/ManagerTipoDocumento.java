package viveresamparito.model.tipodocumento.manager;

import java.util.List;



import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import viveresamparito.model.tipodocumento.manager.ManagerTipoDocumento;
import minimarketdemo.model.core.entities.TipoDeDocumento;

/**
 * Session Bean implementation class ManagerTipoDocumento
 */
@Stateless
@LocalBean
public class ManagerTipoDocumento {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerTipoDocumento() {
		// TODO Auto-generated constructor stub
	}

	public List<TipoDeDocumento> findAllTipoDeDocumentos() {
		String consulta = "select o from TipoDeDocumento o order by o.descripcion";
		Query q = em.createQuery(consulta, TipoDeDocumento.class);
		return q.getResultList();
	}

	public TipoDeDocumento findTipoDeDocumentoByDocumento(int documento) {
		return em.find(TipoDeDocumento.class, documento);
	}

	public void insertarTipoDocumento(TipoDeDocumento documento) throws Exception {
		if (findTipoDeDocumentoByDocumento(documento.getIdTipoDocumento()) != null)
			throw new Exception("Ya existe el tipo de documento ingresado.");
		em.persist(documento);
	}

	public void eliminarTipoDocumento(int documento) {
		TipoDeDocumento tipodocumento = findTipoDeDocumentoByDocumento(documento);
		if (tipodocumento != null)
			em.remove(tipodocumento);
	}

	public void actualizarTipoDocumento(TipoDeDocumento tipodocumento) throws Exception {
		TipoDeDocumento e = findTipoDeDocumentoByDocumento(tipodocumento.getIdTipoDocumento());
		if (e == null)
			throw new Exception("No existe el documento con el id especificado.");
		e.setDescripcion(tipodocumento.getDescripcion());
		em.merge(e);

	}
}
