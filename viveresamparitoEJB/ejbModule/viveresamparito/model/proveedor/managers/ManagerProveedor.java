package viveresamparito.model.proveedor.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CiuCiudad;
import minimarketdemo.model.core.entities.ProProveedor;
import minimarketdemo.model.core.entities.TipoDeDocumento;

/**
 * Session Bean implementation class ManagerProveedor
 */
@Stateless
@LocalBean
public class ManagerProveedor {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerProveedor() {
        // TODO Auto-generated constructor stub
    }
    public List<TipoDeDocumento> findAllTipoDeDocumentos(){
    	TypedQuery<TipoDeDocumento> q = em.createQuery("select o from TipoDeDocumento o order by o.descripcion", TipoDeDocumento.class);
    	return q.getResultList();
    }
    
    public List<CiuCiudad> findAllCiuCiudads(){
    	TypedQuery<CiuCiudad> q = em.createQuery("select o from CiuCiudad o order by o.codigoCiudad ASC", CiuCiudad.class);
    	return q.getResultList();
    }
    public List<ProProveedor> findAllProProveedors(){
    	TypedQuery<ProProveedor> q = em.createQuery("select o from ProProveedor o order by o.cedulaProveedor", ProProveedor.class);
    	return q.getResultList();
    }

	
	public ProProveedor findProveedorByCedula(String cedula) {
		return em.find(ProProveedor.class, cedula);
	}

	public void insertarProductos(ProProveedor descripcion, int idtipoproductoseleccionado, int idproveedorseleccionado) {
		TipoDeDocumento tipoDocumento = em.find(TipoDeDocumento.class, idtipoproductoseleccionado);
		CiuCiudad ciudad = em.find(CiuCiudad.class, idproveedorseleccionado);
		if  (findProveedorByCedula(descripcion.getCedulaProveedor()) != null)
			System.out.println("Ya existe el producto indicado.");
			descripcion.setCiuCiudad(ciudad);
			descripcion.setTipoDeDocumento(tipoDocumento);
			em.persist(descripcion);
		
	}

	public void eliminarProducto(String cedula) {
		ProProveedor proveedor = findProveedorByCedula(cedula);
		if (proveedor != null)
			em.remove(proveedor);
	}
	public void actualizarProveedor(ProProveedor proveedor) throws Exception {
		ProProveedor e =findProveedorByCedula(proveedor.getCedulaProveedor());
		if (e == null)
			throw new Exception("No existe el documento con el id especificado.");
		e.setNombres(proveedor.getNombres());
		e.setCedulaProveedor(proveedor.getCedulaProveedor());
		e.setApellidos(proveedor.getApellidos());
		e.setDireccion(proveedor.getDireccion());
		e.setNombreComercial(proveedor.getNombreComercial());
		e.setTelefono(proveedor.getTelefono());
		em.merge(e);
	}
	

}
