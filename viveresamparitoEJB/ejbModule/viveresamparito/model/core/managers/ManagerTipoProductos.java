package viveresamparito.model.core.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.TipoProducto;

/**
 * Session Bean implementation class ManagerProductos
 */
@Stateless
@LocalBean
public class ManagerTipoProductos {
	@PersistenceContext
	 private EntityManager em;
	 

    /**
     * Default constructor. 
     */
    public ManagerTipoProductos() {
        // TODO Auto-generated constructor stub
    }
    public List<TipoProducto> findAllTipoProductos(){
        String  consulta="select o from TipoProducto o order by  o.idTipoProducto ASC";
        Query q=em.createQuery(consulta, TipoProducto.class);
        return q.getResultList();
    }
    public TipoProducto findTipoProductosByNombre(int idtipoproducto) {
		return em.find(TipoProducto.class, idtipoproducto);
	}
    public void insertarProducto(TipoProducto descripcion) throws Exception {
		if (findTipoProductosByNombre(descripcion.getIdTipoProducto()) != null)
			throw new Exception("Ya existe el producto indicada.");
		em.persist(descripcion);
	}
    public void eliminarProducto(int descripcion) {
		TipoProducto producto = findTipoProductosByNombre(descripcion);
		if (producto != null)
			em.remove(producto);
	}
    public void actualizarTipoProducto(TipoProducto descripcion) throws Exception {
        TipoProducto e = findTipoProductosByNombre(descripcion.getIdTipoProducto());
        if (e == null)
            throw new Exception("No existe el tipo de producto con el id especificado.");
        e.setDescripcion(descripcion.getDescripcion());
        em.merge(e);

    }
}
