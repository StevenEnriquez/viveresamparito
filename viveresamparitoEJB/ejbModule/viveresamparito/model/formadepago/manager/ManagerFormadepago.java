package viveresamparito.model.formadepago.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.FormaDePago;

/**
 * Session Bean implementation class ManagerFormadepago
 */
@Stateless
@LocalBean
public class ManagerFormadepago {
	@PersistenceContext 
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public ManagerFormadepago() {
        // TODO Auto-generated constructor stub
    }
    public List<FormaDePago> findAllTipoDeDocumentos() {
		String consulta = "select o from FormaDePago o order by o.idFormaPago";
		Query q = em.createQuery(consulta, FormaDePago.class);
		return q.getResultList();
	}

	public FormaDePago findTipoDeDocumentoByDocumento(int documento) {
		return em.find(FormaDePago.class, documento);
	}
  
	public void insertarTipoDocumento(FormaDePago documento) throws Exception {
		if (findTipoDeDocumentoByDocumento(documento.getIdFormaPago()) != null)
			throw new Exception("Ya existe el tipo de documento ingresado.");
		em.persist(documento);
	}
 
	public void eliminarTipoDocumento(int documento) {
		FormaDePago tipodocumento = findTipoDeDocumentoByDocumento(documento);
		if (tipodocumento != null)
			em.remove(tipodocumento);
	}

	public void actualizarTipoDocumento(FormaDePago tipodocumento) throws Exception {
		FormaDePago e = findTipoDeDocumentoByDocumento(tipodocumento.getIdFormaPago());
		if (e == null)
			throw new Exception("No existe el documento con el id especificado.");
		e.setDescripcion(tipodocumento.getDescripcion());
		em.merge(e);

	}
}


