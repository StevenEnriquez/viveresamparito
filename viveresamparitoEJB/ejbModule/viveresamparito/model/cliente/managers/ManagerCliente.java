package viveresamparito.model.cliente.managers;



import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CiuCiudad;
import minimarketdemo.model.core.entities.CliCliente;
import minimarketdemo.model.core.entities.TipoDeDocumento;






/**
 * Session Bean implementation class ManagerInscripciones
 */
@Stateless
@LocalBean
public class ManagerCliente {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerCliente() {
		// TODO Auto-generated constructor stub
	}
	
	public List<TipoDeDocumento> findAllTipoDeDocumentos(){
    	TypedQuery<TipoDeDocumento> q = em.createQuery("select o from TipoDeDocumento o order by o.descripcion", TipoDeDocumento.class);
    	return q.getResultList();
    }
    
    public List<CiuCiudad> findAllCiuCiudads(){
    	TypedQuery<CiuCiudad> q = em.createQuery("select o from CiuCiudad o order by o.codigoCiudad ASC", CiuCiudad.class);
    	return q.getResultList();
    }
    public List<CliCliente> findAllCliClientes(){
    	TypedQuery<CliCliente> q = em.createQuery("select o from CliCliente o order by o.cedulaCliente", CliCliente.class);
    	return q.getResultList();
    }

	
	public CliCliente findClienteByCedula(String cedula) {
		return em.find(CliCliente.class, cedula);
	}



	public void insertarCliente(CliCliente descripcion, int idtipodocumento, int idciudad) {
		TipoDeDocumento tipoDocumento = em.find(TipoDeDocumento.class, idtipodocumento);
		CiuCiudad ciudad = em.find(CiuCiudad.class, idciudad);
		if  (findClienteByCedula(descripcion.getCedulaCliente()) != null)
			System.out.println("Ya existe el cliente.");
			descripcion.setCiuCiudad(ciudad);
			descripcion.setTipoDeDocumento(tipoDocumento);
			em.persist(descripcion);
		
	}

	public void eliminarCliente(String cedula) {
		CliCliente cliente = findClienteByCedula(cedula);
		if (cliente != null)
			em.remove(cliente);
	}
	public void actualizarCliente(CliCliente cliente) throws Exception {
		CliCliente e =findClienteByCedula(cliente.getCedulaCliente());
		if (e == null)
			throw new Exception("No existe el documento con el id especificado.");
		e.setCedulaCliente(cliente.getCedulaCliente());
		e.setNombres(cliente.getNombres());
		e.setApellidos(cliente.getApellidos());
		e.setDireccion(cliente.getDireccion());
		e.setTelefono(cliente.getTelefono());
		em.merge(e);
	}
	
}



	
	


