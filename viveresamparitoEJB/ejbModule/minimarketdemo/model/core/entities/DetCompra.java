package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the det_compra database table.
 * 
 */
@Entity
@Table(name="det_compra")
@NamedQuery(name="DetCompra.findAll", query="SELECT d FROM DetCompra d")
public class DetCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_det_compra", unique=true, nullable=false)
	private Integer idDetCompra;

	@Column(nullable=false, precision=11, scale=2)
	private BigDecimal cantidad;

	//bi-directional many-to-one association to ComCompra
	@ManyToOne
	@JoinColumn(name="id_detcom_compra", nullable=false)
	private ComCompra comCompra;

	//bi-directional many-to-one association to ProProducto
	@ManyToOne
	@JoinColumn(name="id_detpro_producto", nullable=false)
	private ProProducto proProducto;

	public DetCompra() {
	}

	public Integer getIdDetCompra() {
		return this.idDetCompra;
	}

	public void setIdDetCompra(Integer idDetCompra) {
		this.idDetCompra = idDetCompra;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public ComCompra getComCompra() {
		return this.comCompra;
	}

	public void setComCompra(ComCompra comCompra) {
		this.comCompra = comCompra;
	}

	public ProProducto getProProducto() {
		return this.proProducto;
	}

	public void setProProducto(ProProducto proProducto) {
		this.proProducto = proProducto;
	}

}