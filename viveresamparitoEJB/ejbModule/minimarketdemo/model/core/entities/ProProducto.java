package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the pro_producto database table.
 * 
 */
@Entity
@Table(name="pro_producto")
@NamedQuery(name="ProProducto.findAll", query="SELECT p FROM ProProducto p")
public class ProProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_producto", unique = true, nullable=false)
	private Integer idProducto;

	@Column(name="descripcion", nullable=false)
	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_ingreso", nullable=false)
	private Date fechaIngreso;

	@Column(name="nombre", nullable=false)
	private String nombre;

	@Column(name="precio_costo", nullable=false)
	private BigDecimal precioCosto;

	@Column(name="precio_venta", nullable=false)
	private BigDecimal precioVenta;

	@Column(name="stock", nullable=false)
	private Integer stock;

	//bi-directional many-to-one association to DetCompra
	@OneToMany(mappedBy="proProducto")
	private List<DetCompra> detCompras;

	//bi-directional many-to-one association to DetFactura
	@OneToMany(mappedBy="proProducto")
	private List<DetFactura> detFacturas;

	//bi-directional many-to-one association to ProProveedor
	@ManyToOne
	@JoinColumn(name="cod_proveedor")
	private ProProveedor proProveedor;

	//bi-directional many-to-one association to TipoProducto
	@ManyToOne
	@JoinColumn(name="cod_tipo_producto")
	private TipoProducto tipoProducto;

	public ProProducto() {
	}

	public Integer getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPrecioCosto() {
		return this.precioCosto;
	}

	public void setPrecioCosto(BigDecimal precioCosto) {
		this.precioCosto = precioCosto;
	}

	public BigDecimal getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Integer getStock() {
		return this.stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public List<DetCompra> getDetCompras() {
		return this.detCompras;
	}

	public void setDetCompras(List<DetCompra> detCompras) {
		this.detCompras = detCompras;
	}

	public DetCompra addDetCompra(DetCompra detCompra) {
		getDetCompras().add(detCompra);
		detCompra.setProProducto(this);

		return detCompra;
	}

	public DetCompra removeDetCompra(DetCompra detCompra) {
		getDetCompras().remove(detCompra);
		detCompra.setProProducto(null);

		return detCompra;
	}

	public List<DetFactura> getDetFacturas() {
		return this.detFacturas;
	}

	public void setDetFacturas(List<DetFactura> detFacturas) {
		this.detFacturas = detFacturas;
	}

	public DetFactura addDetFactura(DetFactura detFactura) {
		getDetFacturas().add(detFactura);
		detFactura.setProProducto(this);

		return detFactura;
	}

	public DetFactura removeDetFactura(DetFactura detFactura) {
		getDetFacturas().remove(detFactura);
		detFactura.setProProducto(null);

		return detFactura;
	}

	public ProProveedor getProProveedor() {
		return this.proProveedor;
	}

	public void setProProveedor(ProProveedor proProveedor) {
		this.proProveedor = proProveedor;
	}

	public TipoProducto getTipoProducto() {
		return this.tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

}