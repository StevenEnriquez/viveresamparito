package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ciu_ciudad database table.
 * 
 */
@Entity
@Table(name="ciu_ciudad")
@NamedQuery(name="CiuCiudad.findAll", query="SELECT c FROM CiuCiudad c")
public class CiuCiudad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="codigo_ciudad")
	private Integer codigoCiudad;

	@Column(name="nombre_ciudad")
	private String nombreCiudad;

	//bi-directional many-to-one association to CliCliente
	@OneToMany(mappedBy="ciuCiudad")
	private List<CliCliente> cliClientes;

	//bi-directional many-to-one association to ProProveedor
	@OneToMany(mappedBy="ciuCiudad")
	private List<ProProveedor> proProveedors;

	public CiuCiudad() {
	}

	public Integer getCodigoCiudad() {
		return this.codigoCiudad;
	}

	public void setCodigoCiudad(Integer codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	public String getNombreCiudad() {
		return this.nombreCiudad;
	}

	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}

	public List<CliCliente> getCliClientes() {
		return this.cliClientes;
	}

	public void setCliClientes(List<CliCliente> cliClientes) {
		this.cliClientes = cliClientes;
	}

	public CliCliente addCliCliente(CliCliente cliCliente) {
		getCliClientes().add(cliCliente);
		cliCliente.setCiuCiudad(this);

		return cliCliente;
	}

	public CliCliente removeCliCliente(CliCliente cliCliente) {
		getCliClientes().remove(cliCliente);
		cliCliente.setCiuCiudad(null);

		return cliCliente;
	}

	public List<ProProveedor> getProProveedors() {
		return this.proProveedors;
	}

	public void setProProveedors(List<ProProveedor> proProveedors) {
		this.proProveedors = proProveedors;
	}

	public ProProveedor addProProveedor(ProProveedor proProveedor) {
		getProProveedors().add(proProveedor);
		proProveedor.setCiuCiudad(this);

		return proProveedor;
	}

	public ProProveedor removeProProveedor(ProProveedor proProveedor) {
		getProProveedors().remove(proProveedor);
		proProveedor.setCiuCiudad(null);

		return proProveedor;
	}

}