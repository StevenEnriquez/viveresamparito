package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the forma_de_pago database table.
 * 
 */
@Entity
@Table(name="forma_de_pago")
@NamedQuery(name="FormaDePago.findAll", query="SELECT f FROM FormaDePago f")
public class FormaDePago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_forma_pago")
	private Integer idFormaPago;

	private String descripcion;

	//bi-directional many-to-one association to FacFactura
	@OneToMany(mappedBy="formaDePago")
	private List<FacFactura> facFacturas;

	public FormaDePago() {
	}

	public Integer getIdFormaPago() {
		return this.idFormaPago;
	}

	public void setIdFormaPago(Integer idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<FacFactura> getFacFacturas() {
		return this.facFacturas;
	}

	public void setFacFacturas(List<FacFactura> facFacturas) {
		this.facFacturas = facFacturas;
	}

	public FacFactura addFacFactura(FacFactura facFactura) {
		getFacFacturas().add(facFactura);
		facFactura.setFormaDePago(this);

		return facFactura;
	}

	public FacFactura removeFacFactura(FacFactura facFactura) {
		getFacFacturas().remove(facFactura);
		facFactura.setFormaDePago(null);

		return facFactura;
	}

}