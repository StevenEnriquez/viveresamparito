package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_de_documento database table.
 * 
 */
@Entity
@Table(name="tipo_de_documento")
@NamedQuery(name="TipoDeDocumento.findAll", query="SELECT t FROM TipoDeDocumento t")
public class TipoDeDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_tipo_documento")
	private Integer idTipoDocumento;

	private String descripcion;

	//bi-directional many-to-one association to CliCliente
	@OneToMany(mappedBy="tipoDeDocumento")
	private List<CliCliente> cliClientes;

	//bi-directional many-to-one association to ProProveedor
	@OneToMany(mappedBy="tipoDeDocumento")
	private List<ProProveedor> proProveedors;

	public TipoDeDocumento() {
	}

	public Integer getIdTipoDocumento() {
		return this.idTipoDocumento;
	}

	public void setIdTipoDocumento(Integer idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<CliCliente> getCliClientes() {
		return this.cliClientes;
	}

	public void setCliClientes(List<CliCliente> cliClientes) {
		this.cliClientes = cliClientes;
	}

	public CliCliente addCliCliente(CliCliente cliCliente) {
		getCliClientes().add(cliCliente);
		cliCliente.setTipoDeDocumento(this);

		return cliCliente;
	}

	public CliCliente removeCliCliente(CliCliente cliCliente) {
		getCliClientes().remove(cliCliente);
		cliCliente.setTipoDeDocumento(null);

		return cliCliente;
	}

	public List<ProProveedor> getProProveedors() {
		return this.proProveedors;
	}

	public void setProProveedors(List<ProProveedor> proProveedors) {
		this.proProveedors = proProveedors;
	}

	public ProProveedor addProProveedor(ProProveedor proProveedor) {
		getProProveedors().add(proProveedor);
		proProveedor.setTipoDeDocumento(this);

		return proProveedor;
	}

	public ProProveedor removeProProveedor(ProProveedor proProveedor) {
		getProProveedors().remove(proProveedor);
		proProveedor.setTipoDeDocumento(null);

		return proProveedor;
	}

}