package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cli_cliente database table.
 * 
 */
@Entity
@Table(name="cli_cliente")
@NamedQuery(name="CliCliente.findAll", query="SELECT c FROM CliCliente c")
public class CliCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cedula_cliente")
	private String cedulaCliente;

	private String apellidos;

	private String direccion;

	private String nombres;

	private String telefono;

	//bi-directional many-to-one association to CiuCiudad
	@ManyToOne
	@JoinColumn(name="cod_ciudad")
	private CiuCiudad ciuCiudad;

	//bi-directional many-to-one association to TipoDeDocumento
	@ManyToOne
	@JoinColumn(name="cod_tipo_documento")
	private TipoDeDocumento tipoDeDocumento;

	//bi-directional many-to-one association to FacFactura
	@OneToMany(mappedBy="cliCliente")
	private List<FacFactura> facFacturas;

	public CliCliente() {
	}

	public String getCedulaCliente() {
		return this.cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public CiuCiudad getCiuCiudad() {
		return this.ciuCiudad;
	}

	public void setCiuCiudad(CiuCiudad ciuCiudad) {
		this.ciuCiudad = ciuCiudad;
	}

	public TipoDeDocumento getTipoDeDocumento() {
		return this.tipoDeDocumento;
	}

	public void setTipoDeDocumento(TipoDeDocumento tipoDeDocumento) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	public List<FacFactura> getFacFacturas() {
		return this.facFacturas;
	}

	public void setFacFacturas(List<FacFactura> facFacturas) {
		this.facFacturas = facFacturas;
	}

	public FacFactura addFacFactura(FacFactura facFactura) {
		getFacFacturas().add(facFactura);
		facFactura.setCliCliente(this);

		return facFactura;
	}

	public FacFactura removeFacFactura(FacFactura facFactura) {
		getFacFacturas().remove(facFactura);
		facFactura.setCliCliente(null);

		return facFactura;
	}

}