package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the pro_proveedor database table.
 * 
 */
@Entity
@Table(name="pro_proveedor")
@NamedQuery(name="ProProveedor.findAll", query="SELECT p FROM ProProveedor p")
public class ProProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cedula_proveedor")
	private String cedulaProveedor;

	private String apellidos;

	private String direccion;

	@Column(name="nombre_comercial")
	private String nombreComercial;

	private String nombres;

	private String telefono;

	//bi-directional many-to-one association to ComCompra
	@OneToMany(mappedBy="proProveedor")
	private List<ComCompra> comCompras;

	//bi-directional many-to-one association to ProProducto
	@OneToMany(mappedBy="proProveedor")
	private List<ProProducto> proProductos;

	//bi-directional many-to-one association to CiuCiudad
	@ManyToOne
	@JoinColumn(name="cod_ciudad")
	private CiuCiudad ciuCiudad;

	//bi-directional many-to-one association to TipoDeDocumento
	@ManyToOne
	@JoinColumn(name="cod_tipo_documento")
	private TipoDeDocumento tipoDeDocumento;

	public ProProveedor() {
	}

	public String getCedulaProveedor() {
		return this.cedulaProveedor;
	}

	public void setCedulaProveedor(String cedulaProveedor) {
		this.cedulaProveedor = cedulaProveedor;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<ComCompra> getComCompras() {
		return this.comCompras;
	}

	public void setComCompras(List<ComCompra> comCompras) {
		this.comCompras = comCompras;
	}

	public ComCompra addComCompra(ComCompra comCompra) {
		getComCompras().add(comCompra);
		comCompra.setProProveedor(this);

		return comCompra;
	}

	public ComCompra removeComCompra(ComCompra comCompra) {
		getComCompras().remove(comCompra);
		comCompra.setProProveedor(null);

		return comCompra;
	}

	public List<ProProducto> getProProductos() {
		return this.proProductos;
	}

	public void setProProductos(List<ProProducto> proProductos) {
		this.proProductos = proProductos;
	}

	public ProProducto addProProducto(ProProducto proProducto) {
		getProProductos().add(proProducto);
		proProducto.setProProveedor(this);

		return proProducto;
	}

	public ProProducto removeProProducto(ProProducto proProducto) {
		getProProductos().remove(proProducto);
		proProducto.setProProveedor(null);

		return proProducto;
	}

	public CiuCiudad getCiuCiudad() {
		return this.ciuCiudad;
	}

	public void setCiuCiudad(CiuCiudad ciuCiudad) {
		this.ciuCiudad = ciuCiudad;
	}

	public TipoDeDocumento getTipoDeDocumento() {
		return this.tipoDeDocumento;
	}

	public void setTipoDeDocumento(TipoDeDocumento tipoDeDocumento) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

}