package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the det_factura database table.
 * 
 */
@Entity
@Table(name="det_factura")
@NamedQuery(name="DetFactura.findAll", query="SELECT d FROM DetFactura d")
public class DetFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_det_factura", unique=true, nullable=false)
	private Integer idDetFactura;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(nullable=false, precision=11, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to FacFactura
	@ManyToOne
	@JoinColumn(name="id_detfac_factura", nullable=false)
	private FacFactura facFactura;

	//bi-directional many-to-one association to ProProducto
	@ManyToOne
	@JoinColumn(name="id_det_producto", nullable=false)
	private ProProducto proProducto;

	public DetFactura() {
	}

	public Integer getIdDetFactura() {
		return this.idDetFactura;
	}

	public void setIdDetFactura(Integer idDetFactura) {
		this.idDetFactura = idDetFactura;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public FacFactura getFacFactura() {
		return this.facFactura;
	}

	public void setFacFactura(FacFactura facFactura) {
		this.facFactura = facFactura;
	}

	public ProProducto getProProducto() {
		return this.proProducto;
	}

	public void setProProducto(ProProducto proProducto) {
		this.proProducto = proProducto;
	}

}