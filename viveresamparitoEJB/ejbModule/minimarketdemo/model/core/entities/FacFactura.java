package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the fac_factura database table.
 * 
 */
@Entity
@Table(name="fac_factura")
@NamedQuery(name="FacFactura.findAll", query="SELECT f FROM FacFactura f")
public class FacFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="num_factura", unique=true, nullable=false)
	private Integer numFactura;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_factura", nullable=false)
	private Date fechaFactura;

	@Column(name="nombre_empleado", nullable=false, length=50)
	private String nombreEmpleado;

	@Column(name="total_factura", precision=11, scale=2)
	private BigDecimal totalFactura;

	//bi-directional many-to-one association to DetFactura
	@OneToMany(mappedBy="facFactura")
	private List<DetFactura> detFacturas;

	//bi-directional many-to-one association to CliCliente
	@ManyToOne
	@JoinColumn(name="cod_cliente", nullable=false)
	private CliCliente cliCliente;

	//bi-directional many-to-one association to FormaDePago
	@ManyToOne
	@JoinColumn(name="cod_formapago", nullable=false)
	private FormaDePago formaDePago;

	public FacFactura() {
	}

	public Integer getNumFactura() {
		return this.numFactura;
	}

	public void setNumFactura(Integer numFactura) {
		this.numFactura = numFactura;
	}

	public Date getFechaFactura() {
		return this.fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public String getNombreEmpleado() {
		return this.nombreEmpleado;
	}

	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}

	public BigDecimal getTotalFactura() {
		return this.totalFactura;
	}

	public void setTotalFactura(BigDecimal totalFactura) {
		this.totalFactura = totalFactura;
	}

	public List<DetFactura> getDetFacturas() {
		return this.detFacturas;
	}

	public void setDetFacturas(List<DetFactura> detFacturas) {
		this.detFacturas = detFacturas;
	}

	public DetFactura addDetFactura(DetFactura detFactura) {
		getDetFacturas().add(detFactura);
		detFactura.setFacFactura(this);

		return detFactura;
	}

	public DetFactura removeDetFactura(DetFactura detFactura) {
		getDetFacturas().remove(detFactura);
		detFactura.setFacFactura(null);

		return detFactura;
	}

	public CliCliente getCliCliente() {
		return this.cliCliente;
	}

	public void setCliCliente(CliCliente cliCliente) {
		this.cliCliente = cliCliente;
	}

	public FormaDePago getFormaDePago() {
		return this.formaDePago;
	}

	public void setFormaDePago(FormaDePago formaDePago) {
		this.formaDePago = formaDePago;
	}

}