package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the com_compra database table.
 * 
 */
@Entity
@Table(name="com_compra")
@NamedQuery(name="ComCompra.findAll", query="SELECT c FROM ComCompra c")
public class ComCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_compra", unique=true, nullable=false)
	private Integer idCompra;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Column(name="numero_compra", nullable=false)
	private Integer numeroCompra;

	//bi-directional many-to-one association to ProProveedor
	@ManyToOne
	@JoinColumn(name="id_compra_proveedor", nullable=false)
	private ProProveedor proProveedor;

	//bi-directional many-to-one association to DetCompra
	@OneToMany(mappedBy="comCompra")
	private List<DetCompra> detCompras;

	public ComCompra() {
	}

	public Integer getIdCompra() {
		return this.idCompra;
	}

	public void setIdCompra(Integer idCompra) {
		this.idCompra = idCompra;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getNumeroCompra() {
		return this.numeroCompra;
	}

	public void setNumeroCompra(Integer numeroCompra) {
		this.numeroCompra = numeroCompra;
	}

	public ProProveedor getProProveedor() {
		return this.proProveedor;
	}

	public void setProProveedor(ProProveedor proProveedor) {
		this.proProveedor = proProveedor;
	}

	public List<DetCompra> getDetCompras() {
		return this.detCompras;
	}

	public void setDetCompras(List<DetCompra> detCompras) {
		this.detCompras = detCompras;
	}

	public DetCompra addDetCompra(DetCompra detCompra) {
		getDetCompras().add(detCompra);
		detCompra.setComCompra(this);

		return detCompra;
	}

	public DetCompra removeDetCompra(DetCompra detCompra) {
		getDetCompras().remove(detCompra);
		detCompra.setComCompra(null);

		return detCompra;
	}

}