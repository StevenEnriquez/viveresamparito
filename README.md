# Viveres Amparito

- Sistema de Inventario
- Desarrollado en la Universidad Técnica Del Norte
- Carrera Ingenieria en Software

Viveres Amparito es un ERP con arquitectura monolítica multicapa JavaEE 

Desarrollado en la Plataforma:

- JavaEE
- IDE Eclipse
- Wildfly
- Postgresql

Tomar en cuenta las siguientes consideraciones para importar el proyecto en Eclipse:

Crear la base de datos viveresamparito en la base de datos Postgresql con el script sql ubicado en viveresamparitoEJB/docs.
Crear un datasource en Wildfly denominado viveresamparitoDS.
Crear un datasource en el IDE Eclipse denominado viveresamparitoDS.

# Integrantes

- Steven Enriquez ssenriqueza@utn.edu.ec
- Heinz Delgado hddelgadoo@utn.edu.ec
- Edwin Herrera epherrerag@utn.edu.ec
- Hector Lescano hglescano@utn.edu.ec
